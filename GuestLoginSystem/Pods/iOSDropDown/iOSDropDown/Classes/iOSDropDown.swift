
//
//  iOSDropDown.swift
//
//
//  Created by Jishnu Raj T on 26/04/18.
//  Copyright © 2018 JRiOSdev. All rights reserved.
//
import UIKit

let ACCEPTABLE_EMAILCHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@"

open class DropDown : UITextField{
    
    var arrow : Arrow!
    var table : UITableView!
    var shadow : UIView!
    
    public  var selectedIndex: Int?
    
    
    //MARK: IBInspectable
    
   @IBInspectable public var rowHeight: CGFloat = 30
   @IBInspectable public var rowBackgroundColor: UIColor = .white
   @IBInspectable public var selectedRowColor: UIColor =  UIColor(red: 234, green: 110, blue: 49, alpha: 1.0)
   @IBInspectable public var hideOptionsWhenSelect = true
   @IBInspectable  public var isSearchEnable: Bool = true {
        didSet{
            addGesture()
        }
    }
    
    
    @IBInspectable public var borderColor: UIColor =  UIColor.lightGray {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable public var listHeight: CGFloat = 150{
        didSet {
            
        }
    }
    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat = 5.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    //Variables
    fileprivate  var tableheightX: CGFloat = 100
    fileprivate  var dataArray:NSArray = [[String:String]]() as NSArray
//    fileprivate  var dataArrayList = [[String: String]]()
//    public var optionArrayList = [[String: String]](){
//        didSet{
//            self.dataArrayList = self.optionArrayList
//        }
//    }
    public var optionArray:NSArray = [[String:String]]() as NSArray{
        didSet{
            self.dataArray = self.optionArray
        }
    }
    //MARK: - Search Text from array
    public var optionIds : [Int]?
    var searchText = String() {
        didSet{
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name("sponsorStar"), object: nil)
            self.dataArray = []
             //self.dataArray.removeAll()
            //if searchText.contains(ACCEPTABLE_EMAILCHARACTERS) {
                //print("searchText string exists in string")
                if searchText != "" {
                    print("IF CHECK searchText = \(searchText)")
                    //let searchPredicate = NSPredicate(format: "SELF beginswith[c] %@", searchText)
                    //self.dataArray  = (self.optionArray as NSArray).filtered(using: searchPredicate) as! [String]
                    let pre:NSPredicate = NSPredicate(format: "firstName beginswith[c] %@ OR lastName beginswith[c] %@", searchText,searchText)
                    self.dataArray = self.optionArray.filtered(using: pre) as NSArray
                    
                }
                //print (" Pod update the new change -- self.dataArray = \( self.dataArray )")
                if self.dataArray.count>0{
                    //print("Count is > 0")
                    if !isSelected{
                        showList()
                    }
                    reSizeTable()
                    selectedIndex = nil
                    self.table.reloadData()
                }else{
                    print("Count is 0")
                    commonResetLoad()
                }
//            }else {
//                print("searchText string not exists in string")
//            }
            
        }
    }
    func commonResetLoad(){
        print("commonResetLoad")
        
        if isSelected{
           reSizeTable()
           hideList()
        }
    
        selectedIndex = nil
        self.table.reloadData()
    }
    /*@IBInspectable var arrowSize: CGFloat = 15 {
        didSet{
            let center =  arrow.superview!.center
            arrow.frame = CGRect(x: center.x - arrowSize/2, y: center.y - arrowSize/2, width: arrowSize, height: arrowSize)
        }
    }*/
  
    // Init
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        self.delegate = self
        //self.font = UIFont(name:"Poppins", size: 16)
        
        
    }
    
    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupUI()
        self.delegate = self
        //self.font = UIFont(name:"Poppins", size: 16)
    }
    
    func clearClicked(sender:UIButton)
    {
        print("clearClicked")
        self.text = ""
    }
    //MARK: Closures
    fileprivate var didSelectCompletion: (String, Int ,Int, String, NSDictionary) -> () = {selectedText, index , id , emailId, selectedEmployeeDetails  in }
    fileprivate var TableWillAppearCompletion: () -> () = { }
    fileprivate var TableDidAppearCompletion: () -> () = { }
    fileprivate var TableWillDisappearCompletion: () -> () = { }
    fileprivate var TableDidDisappearCompletion: () -> () = { }
    
    func setupUI () {
       let size = self.frame.height
        let rightView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: size, height: size))
        self.rightView = rightView
        //self.rightViewMode = .always
        
        //let clearButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 20, height: 20))
        //clearButton.setImage(UIImage(named: "btnClose")!, for: UIControlState.normal)
        //clearButton.setTitle("Close", for: UIControlState.normal)
        //clearButton.backgroundColor = UIColor.red
        //self.rightView = clearButton
        
        //clearButton.addTarget(self, action: Selector(("clearClicked:")), for: UIControlEvents.touchUpInside)
        self.clearButtonMode = UITextFieldViewMode.whileEditing

        //self.rightViewMode = UITextFieldViewMode.always
        //self.rightView?.addSubview(clearButton)
        
        
//        let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(textFieldShouldClear(_:)))
//        tapGesture.numberOfTapsRequired = 1
//        clearButton.addGestureRecognizer(tapGesture)
        //clearButton?.addTarget(self, action: #selector(textFieldShouldClear(_:)), for: UIControl.Event.editingChanged)

        
        //let arrowContainerView = UIView(frame: rightView.frame)
        //self.rightView?.addSubview(arrowContainerView)
        //let center = arrowContainerView.center
        //arrow = Arrow(origin: CGPoint(x: center.x - arrowSize/2,y: center.y - arrowSize/2),size: arrowSize)
        //arrowContainerView.addSubview(clearButton)
        addGesture()
    }
    fileprivate func addGesture (){
        let gesture =  UITapGestureRecognizer(target: self, action:  #selector(touchAction))
        if isSearchEnable{
            self.rightView?.addGestureRecognizer(gesture)
        }else{
            self.addGestureRecognizer(gesture)
        }
        
    }
    
    public func showList() {
        
        //if dataArray.count > 0 {
            TableWillAppearCompletion()
            if listHeight > rowHeight * CGFloat( dataArray.count) {
                self.tableheightX = rowHeight * CGFloat(dataArray.count)
            }else{
                self.tableheightX = listHeight
            }
            table = UITableView(frame: CGRect(x: self.frame.minX,
                                              y: self.frame.minY,
                                              width: self.frame.width,
                                              height: self.frame.height))
//            shadow = UIView(frame: CGRect(x: self.frame.minX,
//                                          y: self.frame.minY,
//                                          width: self.frame.width,
//                                          height: self.frame.height))
//            shadow.backgroundColor = .clear
        
            table.dataSource = self
            table.delegate = self
            table.alpha = 0
            table.separatorStyle = .none
            table.layer.cornerRadius = 3
            table.backgroundColor = rowBackgroundColor
            table.rowHeight = rowHeight
        
            //self.superview?.insertSubview(shadow, belowSubview: self)
            self.superview?.insertSubview(table, belowSubview: self)
            self.isSelected = true
            UIView.animate(withDuration: 0.9,
                           delay: 0,
                           usingSpringWithDamping: 0.4,
                           initialSpringVelocity: 0.1,
                           options: .curveEaseInOut,
                           animations: { () -> Void in
                            
                            self.table.frame = CGRect(x: self.frame.minX,
                                                      y: self.frame.maxY+5,
                                                      width: self.frame.width,
                                                      height: self.tableheightX)
                            
                            self.table.alpha = 1
                            //self.shadow.frame = self.table.frame
                            //self.shadow.dropShadow()
                            //self.arrow.position = .up
                            
            },
                           completion: { (finish) -> Void in
                            
            })
        //}
    }
    
    
    public func hideList() {
        
        TableWillDisappearCompletion()
        UIView.animate(withDuration: 1.0,
                       delay: 0.4,
                       usingSpringWithDamping: 0.9,
                       initialSpringVelocity: 0.1,
                       options: .curveEaseInOut,
                       animations: { () -> Void in
                        self.table.frame = CGRect(x: self.frame.minX,
                                                  y: self.frame.minY,
                                                  width: self.frame.width,
                                                  height: 0)
                        //self.shadow.alpha = 0
                        //self.shadow.frame = self.table.frame
                        //self.arrow.position = .down
        },
                       completion: { (didFinish) -> Void in
                        
                        //self.shadow.removeFromSuperview()
                        self.table.removeFromSuperview()
                        self.isSelected = false
                        self.TableDidDisappearCompletion()
                        self.dataArray = []
                       //self.dataArray.removeAll()
        })
    }
    
    @objc public func touchAction() {
        
        if self.dataArray.count>0{
            isSelected ?  hideList() : showList()
        }

    }
    func reSizeTable() {
        if listHeight > rowHeight * CGFloat( dataArray.count) {
            self.tableheightX = rowHeight * CGFloat(dataArray.count)
        }else{
            self.tableheightX = listHeight
        }
        UIView.animate(withDuration: 0.2,
                       delay: 0.1,
                       usingSpringWithDamping: 0.9,
                       initialSpringVelocity: 0.1,
                       options: .curveEaseInOut,
                       animations: { () -> Void in
                        self.table.frame = CGRect(x: self.frame.minX,
                                                  y: self.frame.maxY+5,
                                                  width: self.frame.width,
                                                  height: self.tableheightX)
                        
        },
                       completion: { (didFinish) -> Void in
                        //self.shadow.layer.shadowPath = UIBezierPath(rect: self.table.bounds).cgPath
                        
        })
    }
    
    //MARK: Actions Methods
    public func didSelect(completion: @escaping (_ selectedText: String, _ index: Int , _ id:Int, _ emailId: String, _ selectedEmployeeDetails : NSDictionary) -> ()) {
        didSelectCompletion = completion
    }
    
    public func listWillAppear(completion: @escaping () -> ()) {
        TableWillAppearCompletion = completion
    }
    
    public func listDidAppear(completion: @escaping () -> ()) {
        TableDidAppearCompletion = completion
    }
    
    public func listWillDisappear(completion: @escaping () -> ()) {
        TableWillDisappearCompletion = completion
    }
    
    public func listDidDisappear(completion: @escaping () -> ()) {
        TableDidDisappearCompletion = completion
    }
    
}

//MARK: UITextFieldDelegate
extension DropDown : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        superview?.endEditing(true)
        return false
    }
    public func  textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.text = ""
        //self.selectedIndex = nil
        //self.dataArray = self.optionArray
//        let nc = NotificationCenter.default
//        nc.post(name: Notification.Name("sponsorStar"), object: nil)
        //self.dataArray.removeAll()
         self.dataArray = []
        touchAction()
    }
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        let nc = NotificationCenter.default
//        nc.post(name: Notification.Name("sponsorStar"), object: nil)
        return isSearchEnable
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("Dropdrop textFieldShouldClear = \(String(describing: textField.text)), searchTex = \(searchText)")
//        let nc = NotificationCenter.default
//        nc.post(name: Notification.Name("sponsorStar"), object: nil)
//        if ((searchText.isEmpty) || ((textField.text?.isEmpty)!)){
//            //commonResetLoad()
//            superview?.endEditing(true)
//        }
        return true
    }
    public func textFieldEditingDidChange(_ textField: UITextField) {
        print("Dropdrop textFieldEditingDidChange")
//        let nc = NotificationCenter.default
//        nc.post(name: Notification.Name("sponsorStar"), object: nil)
        if (textField.text?.isEmpty)!{
            commonResetLoad()
            superview?.endEditing(true)
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
   
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        //prospectiveText.containsOnlyCharactersIn(matchCharacters: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ")
        //print("prospectiveText = \(prospectiveText)")
        
        if (string != "") {
            self.searchText = self.text! + string
        }else{
            let subText = self.text?.dropLast()
            self.searchText = String(subText!)
        }
        
        if !isSelected {
            if self.dataArray.count>0{
                showList()
            }
        }
        if textField.text?.last == " "  && string == " "{
            print("no text")
            // If consecutive spaces entered by user
            return false
        }

        return prospectiveText.containsOnlyCharactersIn(matchCharacters:ACCEPTABLE_EMAILCHARACTERS) &&
            prospectiveText.count <= 62
        
    /*let cs = NSCharacterSet(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
    let filtered = string.components(separatedBy: cs).joined(separator: "")
    let currentString: NSString = textField.text! as NSString
    let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
       return ((newString.length <= 32)&&(string as String == filtered))*/
    }
    
}
///MARK: UITableViewDataSource
extension DropDown: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "DropDownCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        
        if indexPath.row != selectedIndex{
            cell!.backgroundColor = rowBackgroundColor
        }else {
            cell?.backgroundColor = selectedRowColor
        }
        
        cell!.textLabel!.font = UIFont(name: "Poppins-Light", size: 18)
        
        let dicFullNames = dataArray[indexPath.row] as! NSDictionary
        let empFirstName = dicFullNames["firstName"] as! String
        let empLastName = dicFullNames["lastName"] as! String
        let strFullName = "\(empFirstName) \(empLastName)"

        cell!.textLabel!.text = strFullName
        cell!.accessoryType = indexPath.row == selectedIndex ? .checkmark : .none
        cell!.selectionStyle = .none
        //cell?.textLabel?.font = self.font
        cell?.textLabel?.textAlignment = self.textAlignment
        return cell!
    }
}
//MARK: UITableViewDelegate
extension DropDown: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = (indexPath as NSIndexPath).row
        let selectedText = self.dataArray[self.selectedIndex!] as! NSDictionary
        print("selectedText = \(selectedText),, selectedIndex = \(String(describing: selectedIndex))")
        
        let empFirstName = selectedText["firstName"] as! String
        let empLastName = selectedText["lastName"] as! String
        let empEmailID = selectedText["employeeEmailID"] as! String
        let strFullName = "\(empFirstName) \(empLastName)"
        
        tableView.cellForRow(at: indexPath)?.alpha = 0
        UIView.animate(withDuration: 0.5,
                       animations: { () -> Void in
                        tableView.cellForRow(at: indexPath)?.alpha = 1.0
                        tableView.cellForRow(at: indexPath)?.textLabel?.textColor = self.selectedRowColor
                        //tableView.cellForRow(at: indexPath)?.textLabel?.textColor = self.selectedRowColor
                        //tableView.cellForRow(at: indexPath)?.tintColor = UIColor(red: 234, green: 110, blue: 49, alpha: 1.0)

        } ,
                       completion: { (didFinish) -> Void in
                        self.text = "\(strFullName)"
                        tableView.reloadData()
        })
        if hideOptionsWhenSelect {
            //touchAction()
            //self.endEditing(true)
        }
     
        if let id = optionIds?[selectedIndex!] {
            didSelectCompletion(strFullName, selectedIndex! , id , empEmailID, selectedText)
        }else{
            didSelectCompletion(strFullName, selectedIndex! , 0 , empEmailID, selectedText)
        }
        
        /*if let selected = optionArray.index(where: {$0 == selectedText}) {
            if let id = optionIds?[selected] {
                didSelectCompletion(selectedText, selected , id )
            }else{
                didSelectCompletion(selectedText, selected , 0)
            }
            
        }*/
        
    }
}

//MARK: Arrow
enum Position {
    case left
    case down
    case right
    case up
}

class Arrow: UIView {
    
    var position: Position = .down {
        didSet{
            switch position {
            case .left:
                self.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
                break
                
            case .down:
                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi*2)
                break
                
            case .right:
                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
                break
                
            case .up:
                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                break
            }
        }
    }
    
    init(origin: CGPoint, size: CGFloat) {
        super.init(frame: CGRect(x: origin.x, y: origin.y, width: size, height: size))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        
        // Get size
        let size = self.layer.frame.width
        
        // Create path
        let bezierPath = UIBezierPath()
        
        // Draw points
        let qSize = size/4
        
        bezierPath.move(to: CGPoint(x: 0, y: qSize))
        bezierPath.addLine(to: CGPoint(x: size, y: qSize))
        bezierPath.addLine(to: CGPoint(x: size/2, y: qSize*3))
        bezierPath.addLine(to: CGPoint(x: 0, y: qSize))
        bezierPath.close()
        
        // Mask to path
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = bezierPath.cgPath
        if #available(iOS 12.0, *) {
        self.layer.addSublayer (shapeLayer)
        } else {
         self.layer.mask = shapeLayer
        }
    }
}

extension UIView {
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowRadius = 2
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
}
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return self.rangeOfCharacter(from: disallowedCharacterSet) == nil
    }
}
