
import UIKit
import AVFoundation

class CameraViewController: UIViewController {

    //=============
    // MARK: Outlets
    //=============
    @IBOutlet weak var cameraView: UIView! {
        didSet {
            cameraView.layer.mask = maskLayer
        }
    }

    //===================
    // MARK: Lazy Loadings
    //===================
    lazy var cameraManager: CameraManager = {
        let this = CameraManager()
        return this
    }()

    lazy var maskLayer: CALayer = {
        let this = CALayer()
        this.backgroundColor = UIColor(red: 0/255,green: 0/255, blue: 0/255, alpha: 0.5).cgColor
        this.addSublayer(rectLayer)
        return this
    }()

    lazy var rectLayer: CAShapeLayer = {
        let this = CAShapeLayer()
        this.fillColor = UIColor.black.cgColor
        this.strokeColor = UIColor.white.cgColor
        return this
    }()

    lazy var rectPath: UIBezierPath = {
        let this = UIBezierPath()
        return this
    }()

    lazy var useFrontTextLayer: CATextLayer = {
        let this = CATextLayer()
        return this
    }()

    lazy var tapHereToCaptureTextLayer: CATextLayer = {
        let this = CATextLayer()
        return this
    }()

}

extension CameraViewController {

    //=================
    // MARK: - Overrides
    //=================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        //self.view.layer.cornerRadius = 15
        self.view.layer.shadowColor = UIColor.black.cgColor
        self.view.layer.shadowOffset = CGSize(width: 0, height: 10)
        self.view.layer.shadowOpacity = 0.9
       // self.view.layer.shadowRadius = 5
        //showAnimate()
        do {
            
            try cameraManager.captureSetup(in: cameraView, withPosition: .front)
        } catch {
            let alertController = UIAlertController(title: "Error",
                                                    message: error.localizedDescription,
                                                    preferredStyle: .alert)
            alertController.addAction(.init(title: "Ok", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cameraManager.startRunning()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopRunning()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?
            .interactivePopGestureRecognizer?
            .isEnabled = true
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        cameraManager.transitionCamera()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cameraManager.updatePreviewFrame()
        drawOverRectView()
    }
    @IBAction func backButton(_ sender:Any){
        dismissCameraController()
    }
    func dismissCameraController(){
        //self.removeAnimate()
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion:nil)
    }
}

extension CameraViewController {

    //================
    // MARK: - Methods
    //================
    func captureAndCropp() {
        print("crop image frame \(self.rectLayer.frame)")
        cameraManager.getImage(croppWith: self.rectLayer.frame) { (croppedImage, _) -> Void in
            guard let croppedImage = croppedImage else {
                return
            }
             AppDelegate.appdelegateInstance().flagImageCapture = true
            let data = croppedImage.fixOrientation().pngData()
            UserDefaults.standard.set(data, forKey: "myImageKey")
            UserDefaults.standard.synchronize()
            self.dismissCameraController()
        }
    }

    /// this func will draw a rect mask over the camera view
    func drawOverRectView() {

        let cameraSize = self.cameraView.frame.size
        // to calculate the height of frame based on screen size
        let frameHeight: CGFloat
        // to calculate the width of frame based on screen size
        let frameWidth: CGFloat
        // to calculate position Y of recFrame to be in center of cameraView
        let originY: CGFloat
        // to calculate position X of recFrame to be in center of cameraView
        let originX: CGFloat

        let currentDevice: UIDevice = UIDevice.current
        let orientation: UIDeviceOrientation = currentDevice.orientation

        // calculatin position and frame of rectFrame based on screen size
        switch orientation {
            
        case .landscapeRight, .landscapeLeft:
            frameHeight = (cameraSize.height)/1.4
            frameWidth = cameraSize.width/1.5
        default:
            //if it is faceUp or portrait or any other orientation
            frameHeight = (cameraSize.height)/1.25
            frameWidth = cameraSize.width/1.25
        }
        originY = ((cameraSize.height - frameHeight)/2)
        originX = (cameraSize.width - frameWidth)/2

        //create a rect shape layer
        rectLayer.frame = CGRect(x: originX,y: originY, width: frameWidth, height: frameHeight)

        let bezierPathFrame = CGRect(origin: .zero,
                                     size: rectLayer.frame.size)
        print("bezierPathFrame \(bezierPathFrame)")
        //add beizier to rect shapelayer
        rectLayer.path = UIBezierPath(roundedRect: bezierPathFrame,
                                      cornerRadius: 10)
            .cgPath

        //add shapelayer to layer
        maskLayer.frame = cameraView.bounds
    }

}

@objc extension CameraViewController {

    //=================
    // MARK: - Selectors
    //=================

    @IBAction func buttonCapturePhoto(_ sender: Any) {
        captureAndCropp()
    }

}
extension UIImage {
    
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x:0, y:0, width:self.size.width, height:self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }
}
