//
//  ThankYouController.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 15/10/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import UIKit

class ThankYouController: UIViewController {

    var getGuest: GuestDetailsModel! = nil
    
    @IBOutlet weak var lblThankYou:UILabel!
    @IBOutlet weak var lblSuccessMsg:UILabel!
    @IBOutlet weak var btnDone:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.blackTranslucent
        nav?.tintColor = UIColor.white
        nav?.barTintColor =  UIColor.white
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        let guestName = getGuest.guestName!
        
        let txtThankyou = NSLocalizedString("Thank you", comment: "")
        let txtMsgTwo = NSLocalizedString("you have successfully registered", comment: "")
        
        btnDone.setTitle(NSLocalizedString("DONE", comment: ""), for: .normal)
        
        lblSuccessMsg.text = txtMsgTwo
        
        let myMutableString1 = NSMutableAttributedString(string: "\(txtThankyou) " as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Poppins-Regular", size: 22.0)!])
        
        let myMutableString2 = NSMutableAttributedString(string: guestName as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Poppins", size: 22.0)!])
            myMutableString2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:guestName.count))
        
        let combination = NSMutableAttributedString()
        combination.append(myMutableString1)
        combination.append(myMutableString2)
        
        // set label Attribute
        lblThankYou.attributedText = combination
        // Do any additional setup after loading the view.
    }

    //MARK:- Button Function
    @IBAction func buttonDoneClick(_ sender: Any){
        
        if AppDelegate.appdelegateInstance().flagGuestSubmit{
            self.dismiss(animated: true, completion: nil)
        }
    }
}
