//
//  ViewController.swift
//  Terralogic Guest Login System
//
//  Created by Nitin  on 8/29/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import MessageUI
import iOSDropDown
import WebKit

class ViewController: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate,UIScrollViewDelegate,UIImagePickerControllerDelegate,WKUIDelegate,UIPrintInteractionControllerDelegate {
    
    //MARK: - UIControlls
    
    @IBOutlet weak var sponsorDropDown: DropDown!
    
    @IBOutlet weak var txtfldFirstName: UITextField!
    @IBOutlet weak var txtfldLastName: UITextField!
    @IBOutlet weak var txtfldCompanyName: UITextField!
    @IBOutlet weak var txtfldMobileNumber: UITextField!
    @IBOutlet weak var txtfldEmailId: UITextField!
    @IBOutlet weak var txtfldSponsor: UITextField!
    @IBOutlet weak var purposeAutoTextField: UITextField!
    @IBOutlet weak var containerView: UIView!
   
    @IBOutlet weak var btnLearnMore:UIButton!
    @IBOutlet weak var btnResetFields:UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    var activityIndicatorView: UIView!
    
     @IBOutlet weak var imgViewCheckBox: UIImageView!
    
     @IBOutlet weak var imgViewEditPic: UIImageView!
     @IBOutlet weak var imgViewStarFirstName: UIImageView!
     @IBOutlet weak var imgViewStarLastName: UIImageView!
     @IBOutlet weak var imgViewStarCompanyName: UIImageView!
     @IBOutlet weak var imgViewStarMobile: UIImageView!
     @IBOutlet weak var imgViewStarEmail: UIImageView!
     @IBOutlet weak var imgViewStarPurpose: UIImageView!
     @IBOutlet weak var imgViewStarSponsor: UIImageView!
 
      @IBOutlet weak var lblTitle:UILabel!
     @IBOutlet weak var lblEditPic:UILabel!
     @IBOutlet weak var lblLineFirstName:UILabel!
     @IBOutlet weak var lblLineLastName:UILabel!
     @IBOutlet weak var lblLineCompanyName:UILabel!
     @IBOutlet weak var lblLineMobile:UILabel!
     @IBOutlet weak var lblLineEmail:UILabel!
     @IBOutlet weak var lblLinePurpose:UILabel!
     @IBOutlet weak var lblLineSponsor:UILabel!
 
    @IBOutlet weak var lblTrialVersion: UILabel!
    
    @IBOutlet weak var viewSponser:UIView!
    @IBOutlet weak var viewPurpose:UIView!
    @IBOutlet weak var guestImageView: UIImageView!
    
    var imagePicker : UIImagePickerController!
    var flagCheckImageChoose: Bool = false
    var flagPrinterStatus: Bool = false
    var flagE_Mail_SendInBG_Status: Bool = false
    
    @IBOutlet weak var imgViewFirstName: UIImageView!
    @IBOutlet weak var imgViewLastName: UIImageView!
    
    @IBOutlet weak var imgViewStarFirstName1: UIImageView!
    @IBOutlet weak var imgViewStarLastName1: UIImageView!
    @IBOutlet weak var imgViewStarFirstName2: UIImageView!
    @IBOutlet weak var imgViewStarLastName2: UIImageView!
    
    @IBOutlet weak var viewJapanName: UIView!
    @IBOutlet weak var txtfldFirstName1: UITextField!
    @IBOutlet weak var txtfldFirstName2: UITextField!
    @IBOutlet weak var txtfldLastName1: UITextField!
    @IBOutlet weak var txtfldLastName2: UITextField!
    
    @IBOutlet weak var webPreview: WKWebView!
    var imageURL:URL?
    var base64Str:String?
    var HTMLContent: String!
    
    let placeHolderColor: UIColor = UIColor(red: 255/255,green: 255/255, blue:255/255, alpha: 0.75)
    
    let setLocalNotify = LocalNotification()
    private let customIndicatorLoading = CustomLoadingViewExtension()
    let validateManager = ValidationManager()
    private let filemanagerss = FileManagers()
    private let dateManagers = DateManager()
    
    let placeHolderText = NSLocalizedString("Select_the_purpose", comment: "")
    
    // This constraint ties an element at zero points from the bottom layout guide
    // Constraints
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var activeField: UITextField?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    var mailComposerVC = MFMailComposeViewController()
   
    var flagCheckTermsAndCondition: Bool = false
    
    var flagCheckWhiteSpace: Bool = false
    
    lazy var cameraManager: CameraManager = {
        let this = CameraManager()
        return this
    }()
    
    var employeeFullNameList:NSArray? = [[String:String]]() as NSArray
    
    var getSelectedEmployeeEmailID:String  = ""
    var getSelectedEmpProjectName:String  = ""
    var tapGestureRecognizerTextField: UITapGestureRecognizer!
    
    var customView = UIView()
    var webView = WKWebView()
    var blurEffectView = UIVisualEffectView()
    
    var guestModelObject = GuestDetailsModel()
    
     @IBOutlet weak var btnPopOver: UIButton!
    
    var model = ExampleModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    //MARK:  -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.sponsorDropDown.font = UIFont(name: "Poppins-Medium", size: 18)
        setTermsAndCondition()
        
        purposeAutoTextField.autocorrectionType = .yes
        enableAndDisableStar(flagStar: true)
       
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        lblEditPic.isHidden = false
        mailComposerVC.mailComposeDelegate = self
       
        self.sponsorDropDown.autocapitalizationType = UITextAutocapitalizationType.none
        self.sponsorDropDown.text = AppDelegate.appdelegateInstance().getOptionsEmailId
        self.sponsorDropDown.textColor = UIColor.white
       
        guestImageView.layer.masksToBounds = false
        guestImageView.isUserInteractionEnabled = true
        
        guestImageView.layer.masksToBounds = false
        guestImageView.layer.cornerRadius = guestImageView.frame.width/2
        guestImageView.clipsToBounds = true
        
        self.scrollView.panGestureRecognizer.delaysTouchesBegan = true

        self.activeField?.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControl.Event.editingChanged)
        
        setStarImageColour(value: 1)
        setSubmitColor(value: UIColor.gray,flag: false)
        
        self.loadShowAndHideIndicatorView(flag: true)
            DispatchQueue.main.async {
                // Perform your async code here
                
            FileManagers.getAllEmployeeDetails(strName: enumEmployeeList.India)
            WSHelper.funAPIRequestMethod(typeMethod:"GET",getUrlLink:AppServerLink.appVersion) { result, error in
                 print("result appVersion = \(String(describing: result))")
                //self.requestgetGuestMeetByMeetCode()
             }
            
        }

         getDefaultLanguage()
         updateTrialVersionDayCount()
        
        DispatchQueue.main.async {
            if (UserDefaults.standard.bool(forKey: "TrailVersionExpired")){
                self.callTrialVersionController()
            }
        }
    }
   
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.btnPopOver.setTitle(String(describing: model.direction), for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
  
        NotificationCenter.default.removeObserver(self)
        // Observe keyboard change
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        // Compose Mail
        NotificationCenter.default.addObserver(self, selector: #selector(sendExcelSheetWithMailComposer(_:)), name: .postNotifi, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(notifySponsorStar(_:)), name: .postNotifySponsor, object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(updateEmployeeEmailList(_:)), name: .postNotifyUpdateEmailList, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getSelectedLangauge), name: .postNotifyLanguageReloaded, object: nil)
        
        
        if AppDelegate.appdelegateInstance().flagGuestSubmit {
            activeField?.resignFirstResponder()
            activeField = nil
            clearAllFieldValues()
            guestModelObject.clearAllData()
            setStarImageColour(value: 1)
            self.guestImageView.image = UIImage(named: "Camera")
            flagCheckImageChoose = false
            flagCheckTermsAndCondition = false
            imgViewCheckBox.image = UIImage(named: "TermsAndConditionUnSelect")
            imgViewEditPic.image = UIImage(named: "Plus")
            lblEditPic.isHidden = false
            AppDelegate.appdelegateInstance().flagImageCapture = false
            AppDelegate.appdelegateInstance().flagGuestSubmit = false
            
            guestImageView.layer.borderColor = UIColor.clear.cgColor
            guestImageView.layer.borderWidth = 0.0
            
           getDefaultLanguage()
        }
        
        if AppDelegate.appdelegateInstance().flagImageCapture {
            //Get image
            if let imgData = UserDefaults.standard.object(forKey: "myImageKey") as? NSData {
                let retrievedImg = UIImage(data: imgData as Data)
                self.guestImageView.image = retrievedImg
                flagCheckImageChoose = true
                guestImageView.layer.borderColor = UIColor.clear.cgColor
                guestImageView.layer.borderWidth = 0.0
                imgViewEditPic.image = UIImage(named: "PicEdit")
                lblEditPic.isHidden = true
                checkAllFieldsHasValue()
            }
         }
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        activeField?.resignFirstResponder()
        NotificationCenter.default.removeObserver(self)
    }
    
    
    deinit {
        ////print("deinit  method called")
        NotificationCenter.default.removeObserver(self)
    }
    internal func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action: UIAlertAction!) in
            ////print("check title = \(title)")
            if title == NSLocalizedString("Email Candidates Report", comment: ""){
                self.createExcelSheetAndSendMail()
            }
        })
        activeField?.resignFirstResponder()
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
    }
    

    //MARK: -- PopOver Function
    func getDefaultLanguage(){
        if (UserDefaults.standard.bool(forKey: "flag_Language")) {
            getSelectedLangauge()
        }
    }
    @objc func getSelectedLangauge(){
    
        changeInputView()
        setLangauage()
        setTermsAndCondition()
        clearAllFieldValues()
        updatePlaceHolder()
    }
    func setLangauage(){
        
        self.txtfldFirstName.placeholder = NSLocalizedString("First_name", comment: "")
        self.txtfldLastName.placeholder = NSLocalizedString("Last_name", comment: "")
        self.txtfldCompanyName.placeholder = NSLocalizedString("Company_Name", comment: "")
        self.sponsorDropDown.placeholder = NSLocalizedString("To_Meet", comment: "")
        self.txtfldMobileNumber.placeholder = NSLocalizedString("Contact_number", comment: "")
        self.txtfldEmailId.placeholder = NSLocalizedString("Email_Id", comment: "")
        self.purposeAutoTextField.placeholder = NSLocalizedString("Purpose", comment: "")
        
        self.txtfldFirstName1.placeholder = NSLocalizedString("First_name", comment: "")
        self.txtfldFirstName2.placeholder = NSLocalizedString("First_name", comment: "")
        self.txtfldLastName1.placeholder = NSLocalizedString("Last_name", comment: "")
        self.txtfldLastName2.placeholder = NSLocalizedString("Last_name", comment: "")
        
        updateTrialVersionDayCount()
        
        btnResetFields.setTitle(NSLocalizedString("RESET", comment: ""), for: .normal)
        btnLearnMore.setTitle(NSLocalizedString("Learn_more", comment: ""), for: .normal)
        btnTermsAndCondition.setTitle(NSLocalizedString("Terms_and_condition", comment: ""), for: .normal)
        
        lblEditPic.text =  NSLocalizedString("Take_your_Picture", comment: "")
        lblTitle.text = NSLocalizedString("Guest_Login", comment: "")
        
        setStarImageColour(value: 1)
        setSubmitColor(value: UIColor.gray,flag: false)
    }
    @IBAction func showDirectionPopup(_ sender: UIView) {
        
        activeField?.resignFirstResponder()
        
        let controller = ArrayChoiceTableViewController(Direction.allValues) { (direction) in
            self.model.direction = direction
        }
        controller.preferredContentSize = CGSize(width: 250, height: 180)
        showPopup(controller, sourceView: sender)
    }
    
    func updateTrialVersionDayCount(){
        let string1 = String(format: "%02d", UserDefaults.standard.integer(forKey: "CountDay"))
        lblTrialVersion.text =  NSLocalizedString("TrialVersion", comment: "")
        lblTrialVersion.text?.append(contentsOf: " \(string1)")
    }
    private func showPopup(_ controller: UIViewController, sourceView: UIView) {
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = [.down, .up]
        presentationController.backgroundColor = UIColor.lightGray
        
        self.present(controller, animated: true)
    }
    func requestgetGuestMeetByMeetCode(){
        let getMeetBy = String("\(AppServerLink.getGuestMeetByMeetCode)"+"/999999")
        WSHelper.funAPIRequestMethod(typeMethod:"GET", getUrlLink:getMeetBy) { result, error in
            print("getMeetBy = \(String(describing: result))")
        }
        
        let getStatusMeetBy = String("\(AppServerLink.updateStatusMeetByMeetID)"+"/999999")
        WSHelper.funAPIRequestMethod(typeMethod:"PUT", getUrlLink: getStatusMeetBy) { result, error in
            print("getStatusMeetBy = \(String(describing: result))")
        }
        
    }
    //MARK: - Function Language Input Change
    func changeInputView() {
    
        if(AppDelegate.appdelegateInstance().getSelectedLang == "Japanese"){
            viewJapanName.isHidden = false
            imgViewFirstName.isHidden = true
            imgViewLastName.isHidden = true
            lblLineFirstName.isHidden = true
            txtfldFirstName.isHidden = true
            txtfldLastName.isHidden = true
        }else{
            viewJapanName.isHidden = true
            imgViewFirstName.isHidden = false
            imgViewLastName.isHidden = false
            lblLineFirstName.isHidden = false
            txtfldFirstName.isHidden = false
            txtfldLastName.isHidden = false
        }
    }
    func updatePlaceHolder(){
        
        self.txtfldFirstName.placeHolderColor = placeHolderColor
        self.txtfldLastName.placeHolderColor = placeHolderColor
        self.txtfldCompanyName.placeHolderColor = placeHolderColor
        self.sponsorDropDown.placeHolderColor = placeHolderColor
        self.txtfldMobileNumber.placeHolderColor = placeHolderColor
        self.txtfldEmailId.placeHolderColor = placeHolderColor
        self.purposeAutoTextField.placeHolderColor = placeHolderColor
        
        self.txtfldFirstName1.placeHolderColor = placeHolderColor
        self.txtfldFirstName2.placeHolderColor = placeHolderColor
        self.txtfldLastName1.placeHolderColor = placeHolderColor
        self.txtfldLastName2.placeHolderColor = placeHolderColor
        
    }
    //MARK: - Function
    @objc func notifySponsorStar(_ notification: Notification){
        
        ////print("notifySponsorStar")
        self.imgViewStarSponsor.isHidden = false
        self.getSelectedEmployeeEmailID = ""
        self.imgViewStarSponsor.image = UIImage(named: StarImages.starRed)
    }
    
    @objc func updateEmployeeEmailList(_ notification:Notification){
        ////print("updateEmployeeEmailList")
        employeeFullNameList = AppDelegate.appdelegateInstance().arrGetEmployeeNameList as NSArray
        setUpDropDownListsForSponsor()
        self.loadShowAndHideIndicatorView(flag: false)
    }
 
    //MARK: - Check For Daily reports pending
    func checkDailyEmailReports(){
        
        let getGuestList = Guest.fetchGuestList()
        let formatter = DateFormatter()
        let now = Date()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.string(from:now)
        let resultPredicate = NSPredicate(format: "todayDate < %@", todayDate)
        let sortedData: Array<Any> = getGuestList.filtered(using: resultPredicate)
        if (getGuestList.count != 0 && (sortedData.count != 0))
        {
            let txtTile = NSLocalizedString("Email Candidates Report", comment: "")
            let txtMessage = NSLocalizedString("Please proceed here to send all pending records to operation team...", comment: "")
            showAlert(title: txtTile, message: txtMessage)
        }else{
            self.loadShowAndHideIndicatorView(flag: false)
        }
    }
    
    //MARK: - Setup the dropdownlist
    func setUpDropDownListsForSponsor(){
        self.containerView.bringSubviewToFront(sponsorDropDown)
        
        if let checkArrayCount = employeeFullNameList  //as? [[String:String]] as! NSArray
        {
            sponsorDropDown.optionArray = checkArrayCount
        }
        
        sponsorDropDown.isSearchEnable = true
        sponsorDropDown.listDidDisappear {
        
            self.imgViewStarSponsor.isHidden = false
            if (self.sponsorDropDown.text?.isEmpty)!{
                self.getSelectedEmployeeEmailID = ""
            }
            if(((self.sponsorDropDown.text?.isEmpty)!) || (!self.validateManager.isValidEmail(testStr:self.getSelectedEmployeeEmailID))){
                self.imgViewStarSponsor.image = UIImage(named: StarImages.starRed)
            }else{
                self.imgViewStarSponsor.image = UIImage(named: StarImages.starYellow)
            }
        }
        sponsorDropDown.didSelect{(selectedText , index , id, empEmailID, selectedEmployeeDetails) in
            
            self.getSelectedEmployeeEmailID = empEmailID
            self.sponsorDropDown.text = "\(selectedText)"
            self.imgViewStarSponsor.isHidden = false
            
             if(((self.sponsorDropDown.text?.isEmpty)!) || (!self.validateManager.isValidEmail(testStr:self.getSelectedEmployeeEmailID))){
                self.imgViewStarSponsor.image = UIImage(named: StarImages.starRed)
            }else{
                self.imgViewStarSponsor.image = UIImage(named: StarImages.starYellow)
            }
            self.purposeAutoTextField.becomeFirstResponder()
        }
    }
    
    //MARK: - Terms And Condition
    func setTermsAndCondition(){
        
        for view in customView.subviews {
            view.removeFromSuperview()
        }

        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        customView.frame = CGRect.init(x: 50, y: 50, width: self.view.frame.size.width-50, height: self.view.frame.size.height-100)
        customView.backgroundColor = UIColor.white     //give color to the view
        customView.center = self.view.center
        
        let lblTitle = UILabel()
        lblTitle.text = NSLocalizedString("Terms_And_Conditions", comment: "")
        lblTitle.font = UIFont(name: "Poppins-SemiBold", size: 22)
        lblTitle.textAlignment = .center
        lblTitle.frame = CGRect(x: customView.frame.size.width/2-200, y: 10, width: 400, height: 45)
        customView.addSubview(lblTitle)
        
        let image = UIImage(named: "btnClose") as UIImage?
        let buttonClose = UIButton()
        buttonClose.frame = CGRect(x: customView.frame.size.width-40, y: 15, width: 30, height: 30)
        buttonClose.addTarget(self, action: #selector(closeTermsAndCondition), for: UIControl.Event.touchUpInside)
        buttonClose.setImage(image, for: .normal)
        customView.addSubview(buttonClose)
        
        webView.frame = CGRect.init(x: 10, y: lblTitle.frame.size.height+20, width:customView.frame.size.width-20, height: customView.frame.size.height-80)
        webView.backgroundColor = UIColor.white     //give color to the view
        webView.scrollView.contentInset.top = UIEdgeInsets.zero.top
 
        var getSelectLang = "English"
        switch AppDelegate.appdelegateInstance().getSelectedLang {
        case "Vietnamese":
                getSelectLang = "vietnam"
        case "Japanese":
                getSelectLang = "japanese"
        case "English":
                getSelectLang = "english"
        default:
            print("no match")
        }
        if let htmlPagePath = Bundle.main.path(forResource: getSelectLang, ofType: "html") {
            do {
                let html = try String(contentsOfFile: htmlPagePath)
                let bundleUrl = URL(fileURLWithPath: Bundle.main.bundlePath)
                webView.loadHTMLString(html, baseURL: bundleUrl)
                customView.addSubview(webView)
                
            } catch {
                //print("Getting error in resource file")
            }
        }
        
        self.view.addSubview(blurEffectView)
        self.view.addSubview(customView)
        customView.isHidden = true
        blurEffectView.isHidden = true
    }
    
    @objc func closeTermsAndCondition(){
        customView.isHidden = true
        blurEffectView.isHidden = true
    }
    
   
    //MARK: - Submit button color change
    func setSubmitColor(value: UIColor, flag: Bool){
        
        let submitText = NSAttributedString(string: NSLocalizedString("SUBMIT", comment: ""),attributes: [NSAttributedString.Key.foregroundColor : value])
        btnSubmit.setAttributedTitle(submitText, for: .normal)
        btnSubmit.isUserInteractionEnabled = flag
    }
    //MARK: - Show And Hide ActiveIndicator Loading View
    func loadShowAndHideIndicatorView(flag:Bool){
        if flag {
            activityIndicatorView = customIndicatorLoading.displaySpinner(onView: self.view)
        }else{
            customIndicatorLoading.removeSpinner(spinner:activityIndicatorView)
        }
    }
    
    //MARK: - Button Function
    @IBAction func buttonCamera(_ sender:Any){
        
        activeField?.resignFirstResponder()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            AppDelegate.appdelegateInstance().flagImageCapture = false
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CameraViewControllerID") as! CameraViewController
            self.present(vc, animated: true, completion: nil)
        }else if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func buttonCheckBoxOption(_ sender:Any){
         activeField?.resignFirstResponder()
        if !flagCheckTermsAndCondition {
            flagCheckTermsAndCondition = true
            imgViewCheckBox.image = UIImage(named: "TermsAndConditionSelect")
        }else{
            
            flagCheckTermsAndCondition = false
            imgViewCheckBox.image = UIImage(named: "TermsAndConditionUnSelect")
        }
        checkAllFieldsHasValue()
    }
    @IBAction func buttonResetClick(_ sender:Any){
        clearAllFieldValues()
    }
    
    @IBAction func buttonLearnMore(_ sender:Any){
    
        activeField?.resignFirstResponder()
        customView.isHidden = false
        blurEffectView.isHidden = false
    }
    @IBAction func buttonSubmitClick(_ sender:Any){
        
        activeField?.resignFirstResponder()
        
        //Check Internet Connection
        if (currentReachabilityStatus != .notReachable){
            
           self.loadShowAndHideIndicatorView(flag: true)
            
            let txtTitle = NSLocalizedString("Guest Login Alert", comment: "")
            
            if  (AppDelegate.appdelegateInstance().getSelectedLang == "Japanese") {
                
                if((txtfldFirstName1.text?.isEmpty)! || (txtfldLastName1.text?.isEmpty)! ||  (txtfldFirstName2.text?.isEmpty)! || (txtfldLastName2.text?.isEmpty)! ||  (txtfldFirstName.text?.isEmpty)! || (txtfldLastName.text?.isEmpty)! || (txtfldMobileNumber.text?.isEmpty)! || (purposeAutoTextField.text?.isEmpty)! || (txtfldCompanyName.text?.isEmpty)! || (sponsorDropDown.text?.isEmpty)!)
                {
                    showAlert(title: txtTitle, message: NSLocalizedString("Please enter the mandatory details.", comment: ""))
                    self.loadShowAndHideIndicatorView(flag: false)
                    checkTextFieldHasValue()
                }
            }else {
                
                
                if((txtfldFirstName.text?.isEmpty)! || (txtfldLastName.text?.isEmpty)! || (txtfldMobileNumber.text?.isEmpty)! || (purposeAutoTextField.text?.isEmpty)! || (txtfldCompanyName.text?.isEmpty)! || (sponsorDropDown.text?.isEmpty)!)
                {
                    showAlert(title: txtTitle, message: NSLocalizedString("Please enter the mandatory details.", comment: ""))
                    self.loadShowAndHideIndicatorView(flag: false)
                    checkTextFieldHasValue()
                }
            }
    
            if !validateManager.isCountMobileNumber(phone: txtfldMobileNumber.text!){
                 showAlert(title: txtTitle, message: NSLocalizedString("Please enter Valid Mobile Number.", comment: ""))
                self.loadShowAndHideIndicatorView(flag: false)
                checkTextFieldHasValue()
            }
            else if !validateManager.isValidEmail(testStr:getSelectedEmployeeEmailID){
                showAlert(title: txtTitle, message: NSLocalizedString("Please enter Valid Sponsor Email ID.", comment: ""))
                self.loadShowAndHideIndicatorView(flag: false)
                checkTextFieldHasValue()
            }else  if !flagCheckImageChoose{
                checkTextFieldHasValue()
                showAlert(title: txtTitle, message: NSLocalizedString("Please take your Picture.", comment: ""))
                self.loadShowAndHideIndicatorView(flag: false)
            }else  if !flagCheckTermsAndCondition{
                showAlert(title: txtTitle, message: NSLocalizedString("Please select the Terms and Condition.", comment: ""))
                self.loadShowAndHideIndicatorView(flag: false)
     
            }
            else
            {
                
                if(AppDelegate.appdelegateInstance().getSelectedLang == "Japanese") {
                    
                    guestModelObject.guestFirstName = self.txtfldFirstName2.text
                    guestModelObject.guestLastName = self.txtfldLastName2.text
                    
                }else{
                    guestModelObject.guestFirstName = self.txtfldFirstName.text
                    guestModelObject.guestLastName = self.txtfldLastName.text
                }
                guestModelObject.mobileNumber = self.txtfldMobileNumber.text
                
                if(!(self.txtfldEmailId.text?.isEmpty)!){
                    guestModelObject.guestEmailId = self.txtfldEmailId.text
                }else{
                    guestModelObject.guestEmailId = "N/A"
                }
                
                guestModelObject.purpose =  self.purposeAutoTextField.text
                guestModelObject.sponsorName = self.sponsorDropDown.text
                guestModelObject.companyName = self.txtfldCompanyName.text
                guestModelObject.projectName = ""
                guestModelObject.empMailID = self.getSelectedEmployeeEmailID
                guestModelObject.guestImage =  FileManagers().convertImageTobase64(format:
                    .png, image: self.guestImageView.image!)
                guestModelObject.guestName = "\(String(describing: self.txtfldFirstName.text!)) \(String(describing: self.txtfldLastName.text!))"
                let imageName = "\(String(describing: self.txtfldFirstName.text!))\(String(describing: self.txtfldMobileNumber.text!)).png"
                //print("imageName = \(imageName)------")
                guestModelObject.guestImageName = imageName
                
                 // build your dictionary however appropriate
//                "-image
//                + Mean: Local file attach
//                + title: image
//                + type: file"
                // Save image in local directory
                let success = filemanagerss.saveImage(image: self.guestImageView.image!, personName: imageName)
                print("image save success \(success)")
                
                let path = FileManagers().getSavedImagePath(named: imageName)
                var getImageData: NSData? = nil
                if let cert = NSData(contentsOfFile: path) {
                    getImageData = cert
                    //let parameters = ["Mean":getImageData! as Data,"title":"\(imageName)","type":".png"] as [String : Any]
                    let parameters = ["file":getImageData! as Data] as [String : Any]
                    print("parameters = \(parameters), path \(path)")
        
                    //let parameters = ["Mean":self.guestImageView.image!,"title":"image","type":"file"] as [String : Any]
                    
                    ApiManager.imageUpload(parameters: parameters,imagePath: path)
                    //WSHelper.requestImageUploadPostMethod(uploadLink: AppServerLink.imageUploadLink, imageName: imageName, parameters: param)
                    
                    //WSHelper().funRequestPostImageUpload(uploadLink: AppServerLink.imageUploadLink,imageName: imageName, parameters: param )
                }
                /*let param = ["firstName": "\(String(describing: guestModelObject.guestFirstName))",
                    "lastName": "\(String(describing: guestModelObject.guestLastName))",
                    "avatar": "https://png.pngtree.com/element_our/sm/20180327/sm_5aba147bcacf2.png",
                    "contactNumber":"\(String(describing: guestModelObject.mobileNumber))",
                    "email": "\(String(describing: guestModelObject.empMailID))",
                    "companyName": "\(String(describing: guestModelObject.companyName))",
                    "meetPeople": "855",
                    "meetPurpose": "\(String(describing: guestModelObject.purpose))" ]
                
                print("param = \(param)")
                WSHelper.requestPostMethod(getUrl: AppServerLink.createANewMeetGuest, parameters: param){ result, error in
                       print("createANewMeetGuest result = \(String(describing: result))")
                }*/
            self.loadShowAndHideIndicatorView(flag: false)
            submitDetails(getGuestDetails: guestModelObject)
            //MailComposerManager.sendEMailInBackground(getGuestList: guestModelObject)
  
            }
        }else{
            //print("No Internet Connection")
            showAlert(title: NSLocalizedString("No_Internet_Connection!", comment: ""), message: NSLocalizedString("Please check your connection and try again.", comment: ""))
        }
    }
    
    
    func submitDetails(getGuestDetails: GuestDetailsModel){
        
        let imageLogo = UIImage(named:"logoCard")
        let base64StrLogo = FileManagers().convertImageTobase64(format:
            .png, image: imageLogo!)
        
        base64Str = FileManagers().convertImageTobase64(format:
            .png, image: self.guestImageView.image!)
        
        HTMLContent = HtmlFormat.createVisitorCard(visitorName: getGuestDetails.guestName!, visitorCompanyName: getGuestDetails.companyName!, visitorImage: base64Str!, imageLogo: base64StrLogo!, meetingPersonName: getGuestDetails.sponsorName!) as String
        
        printHTMLFormatCode(getGuestDetails: getGuestDetails)

    }
    //MARK: - Printer Method
    //One more example using HTML string
    func printHTMLFormatCode(getGuestDetails: GuestDetailsModel) {
        
        let printIntaractionController = UIPrintInteractionController.shared
        printIntaractionController.delegate = self
        
        // 2
        let printInfo = UIPrintInfo(dictionary:nil)
        printInfo.outputType = UIPrintInfo.OutputType.photo
        printInfo.jobName = "print Job"
        printIntaractionController.printInfo = printInfo
        let formatter = UIMarkupTextPrintFormatter(markupText: self.HTMLContent!)
        printIntaractionController.printFormatter = formatter
        printIntaractionController.printInfo?.orientation = .landscape
        printIntaractionController.showsNumberOfCopies = true
        printIntaractionController.present(animated: true, completionHandler: {
            controller, completed, error in
            if completed == true {
                
                Swift.print("Completion")
                AppDelegate.appdelegateInstance().flagEditGuestDetails = false
                Guest.insertGuestRecords(guestObj: getGuestDetails)
                AppDelegate.appdelegateInstance().flagGuestSubmit = true
                self.callThankYouController()
            }else {
                Swift.print("Print Cancel")
                self.clearAllFieldValues()
            }
        })
    }
    func callThankYouController(){
        let vcThankyouControll = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ThankYouControllerID") as! ThankYouController
        vcThankyouControll.getGuest = guestModelObject
        self.present(vcThankyouControll, animated: false, completion: nil)
    }
    func callTrialVersionController(){
        
        let vcThankyouControll = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TrialVersionViewControllerID") as! TrialVersionViewController
        self.present(vcThankyouControll, animated: false, completion: nil)
    }
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            
            self.guestImageView.image = pickedImage
            flagCheckImageChoose = true
            //imgViewEditPic.isHidden = false
            imgViewEditPic.image = UIImage(named: "PicEdit")
            lblEditPic.isHidden = true
            //callCustomCropPhotoView(image: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
  //MARK: - TextField Delegate Function
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if textField == txtfldFirstName || textField == txtfldLastName || textField == txtfldFirstName1 || textField == txtfldFirstName2 || textField == txtfldLastName1 || textField == txtfldLastName2{
        
        if textField.text?.last == " "  && string == " "{
            // If consecutive spaces entered by user
            return false
        }
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        if  (AppDelegate.appdelegateInstance().getSelectedLang == "English") {
            return prospectiveText.containsOnlyCharactersIn(matchCharacters: CharactersVaildations.ACCEPTABLE_NAMECHARACTERS) &&
                prospectiveText.count <= 64
        }else {
            return prospectiveText.count <= 64
        }
    }
    else if textField == txtfldMobileNumber{
        
        let cs = NSCharacterSet(charactersIn: CharactersVaildations.ACCEPTABLE_MOBILECHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return ((newString.length <= 10)&&(string as String == filtered))
        
    }else if textField == txtfldEmailId{
        
        if ((textField.text?.last == "@"  && string == "@") || (textField.text?.last == "."  && string == ".") || (textField.text?.last == "_"  && string == "_")){
            // If consecutive spaces entered by user
            return false
        }
        
        if  (AppDelegate.appdelegateInstance().getSelectedLang == "English") {
            
            let cs = NSCharacterSet(charactersIn: CharactersVaildations.ACCEPTABLE_EMAILCHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
            
        }else {
            
            let currentText = textField.text ?? ""
            let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
            return prospectiveText.count <= 32
        }
       
        
    }else if textField == purposeAutoTextField{
        
        if textField.text?.last == " "  && string == " "{
            // If consecutive spaces entered by user
            return false
        }
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        if  (AppDelegate.appdelegateInstance().getSelectedLang == "English") {
            return prospectiveText.containsOnlyCharactersIn(matchCharacters: CharactersVaildations.ACCEPTABLE_NAMECHARACTERS) &&
                prospectiveText.count <= 64
        }else {
            return prospectiveText.count <= 64
        }

    }else if textField == txtfldCompanyName {
        
            if ((textField.text?.last == " "  && string == " ") || (textField.text?.last == ","  && string == ",") || (textField.text?.last == "."  && string == ".")) {
                // If consecutive spaces entered by user
                return false
            }
            let currentText = textField.text ?? ""
            let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
            if  (AppDelegate.appdelegateInstance().getSelectedLang == "English") {
                return prospectiveText.containsOnlyCharactersIn(matchCharacters: CharactersVaildations.ACCEPTABLE_COMPANYCHARATERS) &&
                prospectiveText.count <= 64
            }else {
                return prospectiveText.count <= 64
            }
        }
        return true
    }
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        checkAllFieldsHasValue()
            if textField == txtfldFirstName1 {
                txtfldFirstName2.becomeFirstResponder()
                return true
            }else if textField == txtfldFirstName2{
                txtfldLastName1.becomeFirstResponder()
                return true
            }else if textField == txtfldLastName1{
                txtfldLastName2.becomeFirstResponder()
                return true
            }else if textField == txtfldLastName2{
                txtfldMobileNumber.becomeFirstResponder()
                return true
            }
            else if textField == txtfldFirstName {
                txtfldLastName.becomeFirstResponder()
                return true
            }else if textField == txtfldLastName{
                txtfldMobileNumber.becomeFirstResponder()
                return true
            }
            else if textField == txtfldMobileNumber{
                txtfldEmailId.becomeFirstResponder()
                return true
            }else if textField == txtfldEmailId{
                txtfldCompanyName.becomeFirstResponder()
                return true
            }else if textField == txtfldCompanyName{
                sponsorDropDown.becomeFirstResponder()
                return true
            }else if textField == sponsorDropDown{
                purposeAutoTextField.becomeFirstResponder()
                return true
            }else if textField == purposeAutoTextField{
                purposeAutoTextField.resignFirstResponder()
                return true
            }
        return false
    }
     func textFieldDidBeginEditing(_ textField: UITextField) {
        ////print("TextField did begin editing method called")
     }
    func textFieldDidEndEditing(_ textField: UITextField) {
        ////print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        ////print("textFieldShouldBeginEditing")
        activeField = textField
        
        if activeField != sponsorDropDown{
            if sponsorDropDown.isSelected {
                sponsorDropDown.hideList()
            }
        }
        
        lastOffset = self.scrollView.contentOffset
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        ////print("textFieldShouldEndEditing")
        return true
    }
    @objc func returnTextField(gestureRecognizer: UIGestureRecognizer) {
         ////print("returnTextField UIGestureRecognizer")

        guard activeField != nil else {
            return
        }
        activeField?.resignFirstResponder()
        lastOffset  = self.scrollView.contentOffset
     }
     func textFieldDidChange(_ textField: UITextField) {

    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        ////print("TextField should clear method called")
         return true;
    }
    
    @IBAction func textFieldEditingDidChange(_ sender: Any) {
        setStarImageColour(value: 3)
        checkAllFieldsHasValue()
    }

    //MARK: - Auto Fill TextField
    func autoCompleteText( in textField: UITextField, using string: String, employeesEmailID: [String]) -> Bool {
        if !string.isEmpty,
            let selectedTextRange = textField.selectedTextRange,
            selectedTextRange.end == textField.endOfDocument,
            let prefixRange = textField.textRange(from: textField.beginningOfDocument, to: selectedTextRange.start),
            let text = textField.text( in : prefixRange) {
            let prefix = text + string
            let matches = employeesEmailID.filter {
                $0.hasPrefix(prefix)
            }
            if (matches.count > 0) {
                textField.text = matches[0]
                if let start = textField.position(from: textField.beginningOfDocument, offset: prefix.count) {
                    textField.selectedTextRange = textField.textRange(from: start, to: textField.endOfDocument)
                    return true
                }
            }
        }
        return false
    }
    //MARK: - Local Notification Function
    @objc func sendExcelSheetWithMailComposer(_ notification: Notification){
        self.loadShowAndHideIndicatorView(flag: true)
        createExcelSheetAndSendMail()
    }
    //MARK: - Create Excel Sheet and SendMail Function
    func createExcelSheetAndSendMail(){
        
            if MFMailComposeViewController.canSendMail() {
                let getGuestList = Guest.fetchGuestList() as! Array<Any>
                if getGuestList.count > 0
                {
                    filemanagerss.createDictionaryFormat(arrGetGuestList:getGuestList, completion: {(flagtStatus: Bool) in DispatchQueue.main.async {
                       // //print("Sucessfully generated report\(flagtStatus)")
                        ////print("sendExcelSheetWithMailComposer")
                        let toRecipients = ["rajkumar.m@terralogic.com"]
                        let subject = NSLocalizedString("Guest Records", comment: "")
                        
                        let msgContent:String = "\(NSLocalizedString("Hello,", comment: ""))\n \(NSLocalizedString("Please find all the list of guest details who visted today.", comment: ""))"
                        let htmlBodyMsgTable = "\(String(describing: msgContent))\n\n\(NSLocalizedString("Thank you,", comment: ""))\n\n\(NSLocalizedString("Guest Login System.", comment: ""))"
                        let body = htmlBodyMsgTable
                        let mail = self.sendExcelSheetWithMail(recipients: toRecipients, subject: subject, body: body, isHtml: false, images: nil)
                         self.presentMailComposeViewController(mailComposeViewController: mail)
                        self.loadShowAndHideIndicatorView(flag: false)
                        }
                    })
                }
            }else{
                //print("Mail Failed")
                let txtTitle = NSLocalizedString("Could not sent email", comment: "")
                let txtMessage = NSLocalizedString("Your device could not send e-mail.  Please check e-mail configuration and try again.", comment: "")
                showSendMailErrorAlert(title:txtTitle, message:txtMessage)
                 self.loadShowAndHideIndicatorView(flag: false)
            }
    }

    //MARK:  -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- ImagePickerController

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
