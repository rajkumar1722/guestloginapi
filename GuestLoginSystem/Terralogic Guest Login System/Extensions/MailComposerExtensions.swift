//
//  MailComposerExtensions.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 05/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import Foundation
import MessageUI

extension ViewController : MFMailComposeViewControllerDelegate {
    
    func configuredMailComposeViewController(recipients : [String]?, subject :
        String, body : String, isHtml : Bool = false,
                images : [UIImage]?) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // IMPORTANT
        
        mailComposerVC.setToRecipients(recipients)
        mailComposerVC.setSubject(subject)
        mailComposerVC.setMessageBody(body, isHTML: isHtml)
        
        for img in images ?? [] {
            if let jpegData = img.jpegData(compressionQuality: 1.0) {
                mailComposerVC.addAttachmentData(jpegData,
                                                 mimeType: "image/jpg",
                                                 fileName: "Image")
            }
        }
        return mailComposerVC
    }
    
    func sendExcelSheetWithMail(recipients : [String]?, subject :
        String, body : String, isHtml : Bool = false,
                images : [UIImage]?) -> MFMailComposeViewController  {
        
       
        mailComposerVC.mailComposeDelegate = self
//      Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
//      mailComposerVC.setToRecipients(["rajkumar.m@terralogic.com"])
//      mailComposerVC.setSubject("Sending you an in-app e-mail...")
//      mailComposerVC.setMessageBody("Hi, \(String(describing: textInputTextFld.text))", isHTML: false)

        mailComposerVC.setToRecipients(recipients)
        mailComposerVC.setSubject(subject)
        mailComposerVC.setMessageBody(body, isHTML: isHtml)
        
//        print("fileURL = \(FileManagers.getFileLocation().path)")
        
        if let fileData = NSData(contentsOfFile:FileManagers.getFileLocation().path){
                mailComposerVC.addAttachmentData(fileData as Data, mimeType: "application/vnd.ms-excel", fileName: "GuestsRecords")
            }
        
        return mailComposerVC
    }
  
    func presentMailComposeViewController(mailComposeViewController :
        MFMailComposeViewController) {
        if MFMailComposeViewController.canSendMail() {
            //print("presentMailComposeViewController  MFMailComposeViewController.canSendMail")
            self.present(mailComposeViewController,animated: true, completion: nil)
        } else {
           //print("Mail Failed")
            let txtMailNotSent = NSLocalizedString("Mail not sent", comment: "")
            let txtMessage = NSLocalizedString("Your device could not send an e-mail, Please check e-mail configuration and try again.", comment: "")
           showSendMailErrorAlert(title:txtMailNotSent, message: txtMessage)
        }
    }

    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {

        print("didFinishWith")
        switch (result) {
        case .cancelled:
             print("Mail cancelled")
            self.dismiss(animated: true, completion: nil)
        case .saved:
            print("Mail saved")
            self.dismiss(animated: true, completion: nil)
        case .sent:
            print("Mail sent")
            Guest.deleteAllRecords()
            FileManagers().deleteDirectoryFromHomeDirectory()
            self.dismiss(animated: true, completion: nil)
            break
        case .failed:
            print("Mail Failed")
            self.dismiss(animated: true, completion: {
                let txtTitle = NSLocalizedString("Email Failed!", comment: "")
                let txtMessage = NSLocalizedString("Unable to send email. Please check your email settings and try again.", comment: "")
            self.showSendMailErrorAlert(title:txtTitle, message: txtMessage)
            })
        }
    }
    
    func versionText () -> String {
        let bundleVersionKey = "CFBundleShortVersionString"
        let buildVersionKey = "CFBundleVersion"
        
        if let version = Bundle.main.object(forInfoDictionaryKey: bundleVersionKey) {
            if let build = Bundle.main.object(forInfoDictionaryKey: buildVersionKey) {
                let version = "Version \(version) - Build \(build)"
                return version
            }
        }
        return ""
    }
    //MARK - Alert Method
    func showSendMailErrorAlert(title: String, message: String) {
        let alertMessage = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title:NSLocalizedString("Okay", comment: ""), style: UIAlertAction.Style.default, handler: nil)
        alertMessage.addAction(action)
        self.present(alertMessage, animated: true, completion: nil)
    }
}
