//
//  NotificationNameExtension.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 04/01/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import Foundation
import  UIKit

extension Notification.Name {
    static let postNotifi = Notification.Name("sendExcelSheetWithMailComposer")
    static let postNotifySponsor = Notification.Name("sponsorStar")
    static let postNotifyUpdateEmailList = Notification.Name("updateEmailList")
    static let postNotifyLanguageReloaded = Notification.Name("languageReload")
}
