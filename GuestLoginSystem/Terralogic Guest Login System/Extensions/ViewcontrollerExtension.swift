//
//  ViewcontrollerExtension.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 04/01/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import Foundation
import UIKit

// MARK: Keyboard Handling
extension ViewController {
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //print("keyboardWillShow")
        var info = notification.userInfo
        let keyBoardSize = info![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        scrollView.contentInset = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyBoardSize.height, right: 0.0)
        scrollView.scrollIndicatorInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyBoardSize.height, right: 0.0)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //print("keyboardWillHide")
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        checkAllFieldsHasValue()
        if sponsorDropDown.isSelected {
            sponsorDropDown.hideList()
        }
    }
    @objc func keyboardWillChange(notification: NSNotification) {
        //    self.scrollView.frame.origin.y = -20
    }
}
