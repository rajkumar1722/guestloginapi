//
//  TextFieldExtension.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 04/01/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!, NSAttributedString.Key.font:
                UIFont(name: "Poppins-Medium", size: 18)!])
        }
    }
    
}
