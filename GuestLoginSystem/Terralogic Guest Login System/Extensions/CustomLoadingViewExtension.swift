//
//  CustomLoadingViewExtension.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 04/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import UIKit

class CustomLoadingViewExtension: UIView {

    func displaySpinner(onView : UIView) -> UIView {
        
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.6)
        
        
//        spinnerView.layer.borderColor = UIColor.red.cgColor
//        spinnerView.layer.cornerRadius = 5.0
//        spinnerView.layer.borderWidth = 5.0
        
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.color = UIColor.white
        ai.startAnimating()
        ai.center = spinnerView.center
        
        let lbl = UILabel(frame: CGRect(x: ai.frame.origin.x-15, y: ai.frame.origin.y + ai.frame.size.height+20, width: 250, height: 25))
        lbl.textAlignment = .center //For center alignment
        lbl.text = NSLocalizedString("Loading...", comment: "")
        lbl.textColor = .white
        lbl.font = UIFont(name: "Poppins-Medium", size: 18)
        
        //To display multiple lines in label
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        lbl.sizeToFit()//If required
 
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            spinnerView.addSubview(lbl)
            onView.addSubview(spinnerView)
        }
        return spinnerView
    }
    
    func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
