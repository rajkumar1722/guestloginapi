//
//  ViewControllFieldExtension.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 17/01/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import Foundation
import UIKit

extension  ViewController{
    
    
    func clearAllFieldValues(){
        
        activeField?.resignFirstResponder()
        activeField = nil
        
        self.txtfldFirstName.text = ""
        self.txtfldLastName.text = ""
        
        
        self.txtfldFirstName1.text = ""
        self.txtfldLastName1.text = ""
        self.txtfldFirstName2.text = ""
        self.txtfldLastName2.text = ""
        
        self.txtfldCompanyName.text = ""
        self.sponsorDropDown.text = ""
        self.txtfldMobileNumber.text = ""
        self.txtfldEmailId.text = ""
        self.purposeAutoTextField.text = ""
        
        guestImageView.image = UIImage(named: "Camera.png")
        lblEditPic.isHidden = false
        flagCheckImageChoose = false
        //imgViewEditPic.isHidden = true
        guestImageView.layer.borderColor = UIColor.clear.cgColor
        guestImageView.layer.borderWidth = 0.0
        imgViewCheckBox.image = UIImage(named: "TermsAndConditionUnSelect")
        imgViewEditPic.image = UIImage(named: "Plus")
        setStarImageColour(value: 1)
        flagCheckTermsAndCondition = false
        enableAndDisableStar(flagStar: true)
        setSubmitColor(value: UIColor.gray,flag: false)
    }
    
    func checkTextFieldHasValue(){
        
      
        if((txtfldFirstName1.text?.isEmpty)!) {
            imgViewStarFirstName1.image = UIImage(named: StarImages.starRed)
            imgViewStarFirstName1.isHidden = false
        }else{
            imgViewStarFirstName1.image = UIImage(named: StarImages.starYellow)
            imgViewStarFirstName1.isHidden = false
        }
        if((txtfldLastName1.text?.isEmpty)!) {
            imgViewStarLastName1.image = UIImage(named: StarImages.starRed)
            imgViewStarLastName1.isHidden = false
        }else{
            imgViewStarLastName1.image = UIImage(named: StarImages.starYellow)
            imgViewStarLastName1.isHidden = false
        }
        if((txtfldFirstName2.text?.isEmpty)!) {
            imgViewStarFirstName2.image = UIImage(named: StarImages.starRed)
            imgViewStarFirstName2.isHidden = false
        }else{
            imgViewStarFirstName2.image = UIImage(named: StarImages.starYellow)
            imgViewStarFirstName2.isHidden = false
        }
        if((txtfldLastName2.text?.isEmpty)!) {
            imgViewStarLastName2.image = UIImage(named: StarImages.starRed)
            imgViewStarLastName2.isHidden = false
        }else{
            imgViewStarLastName2.image = UIImage(named: StarImages.starYellow)
            imgViewStarLastName2.isHidden = false
        }        
        
        if((txtfldFirstName.text?.isEmpty)!) {
            imgViewStarFirstName.image = UIImage(named: StarImages.starRed)
            imgViewStarFirstName.isHidden = false
        }else{
            imgViewStarFirstName.image = UIImage(named: StarImages.starYellow)
            imgViewStarFirstName.isHidden = false
        }
        if((txtfldLastName.text?.isEmpty)!) {
            imgViewStarLastName.image = UIImage(named: StarImages.starRed)
            imgViewStarLastName.isHidden = false
        }else{
            imgViewStarLastName.image = UIImage(named: StarImages.starYellow)
            imgViewStarLastName.isHidden = false
        }
        
        if((txtfldCompanyName.text?.isEmpty)!){
            imgViewStarCompanyName.image = UIImage(named: StarImages.starRed)
            imgViewStarCompanyName.isHidden = false
        }else{
            imgViewStarCompanyName.image = UIImage(named: StarImages.starYellow)
            imgViewStarCompanyName.isHidden = false
        }
        if(((txtfldMobileNumber.text?.isEmpty)!) || (!validateManager.isCountMobileNumber(phone: txtfldMobileNumber.text!)))
        {
            imgViewStarMobile.image = UIImage(named: StarImages.starRed)
            imgViewStarMobile.isHidden = false
        }else{
            imgViewStarMobile.image = UIImage(named: StarImages.starYellow)
            imgViewStarMobile.isHidden = false
        }
        if((purposeAutoTextField.text?.isEmpty)!){
            imgViewStarPurpose.image = UIImage(named: StarImages.starRed)
            imgViewStarPurpose.isHidden = false
        }else{
            imgViewStarPurpose.image = UIImage(named: StarImages.starYellow)
            imgViewStarPurpose.isHidden = false
        }
        if(((sponsorDropDown.text?.isEmpty)!) || (!validateManager.isValidEmail(testStr:getSelectedEmployeeEmailID))){
            imgViewStarSponsor.image = UIImage(named: StarImages.starRed)
            imgViewStarSponsor.isHidden = false
        }else{
            imgViewStarSponsor.image = UIImage(named: StarImages.starYellow)
            imgViewStarSponsor.isHidden = false
        }
        
        if !flagCheckImageChoose{
              //print("checkTextFieldHasValue -- flagCheckImageChoose")
            guestImageView.layer.borderColor = UIColor.red.cgColor
            guestImageView.layer.borderWidth = 2.0
        }else{
            guestImageView.layer.borderColor = UIColor.clear.cgColor
            guestImageView.layer.borderWidth = 0.0
        }
        
    }
    func setStarImageColour(value: Int){
        
        if activeField == txtfldFirstName1{
            checkIsValidateTextFieldValue()
        }else if activeField == txtfldLastName1{
            checkIsValidateTextFieldValue()
        }else if activeField == txtfldFirstName2{
            checkIsValidateTextFieldValue()
        }else if activeField == txtfldLastName2{
            checkIsValidateTextFieldValue()
        }
            
        else if activeField == txtfldFirstName{
            checkIsValidateTextFieldValue()
        }else if activeField == txtfldLastName{
            checkIsValidateTextFieldValue()
        }else if activeField == txtfldMobileNumber{
            checkIsValidateTextFieldValue()
        }else if activeField == txtfldCompanyName{
            checkIsValidateTextFieldValue()
        }else if activeField == sponsorDropDown{
            checkIsValidateTextFieldValue()
        }else if activeField == purposeAutoTextField{
            checkIsValidateTextFieldValue()
        }else if ((AppDelegate.appdelegateInstance().flagGuestSubmit == true) || (value == 1)) {
           
            imgViewStarFirstName1.image = UIImage(named: StarImages.starWhite)
            imgViewStarLastName1.image = UIImage(named: StarImages.starWhite)
            imgViewStarFirstName2.image = UIImage(named: StarImages.starWhite)
            imgViewStarLastName2.image = UIImage(named: StarImages.starWhite)
            
            imgViewStarFirstName.image = UIImage(named: StarImages.starWhite)
            imgViewStarLastName.image = UIImage(named: StarImages.starWhite)
            imgViewStarCompanyName.image = UIImage(named: StarImages.starWhite)
            imgViewStarMobile.image = UIImage(named: StarImages.starWhite)
            imgViewStarEmail.image = UIImage(named: StarImages.starWhite)
            imgViewStarPurpose.image = UIImage(named: StarImages.starWhite)
            imgViewStarSponsor.image = UIImage(named: StarImages.starWhite)
        }
    }
    func checkIsValidateTextFieldValue(){
       
        
        if (activeField == txtfldFirstName1) {
            txtfldFirstName1.text = txtfldFirstName1.text?.capitalizingFirstLetter()
            if((txtfldFirstName1.text?.isEmpty)!) {
                imgViewStarFirstName1.image = UIImage(named: StarImages.starRed)
                imgViewStarFirstName1.isHidden = false
            }else{
                imgViewStarFirstName1.image = UIImage(named: StarImages.starYellow)
                imgViewStarFirstName1.isHidden = false
            }
        }else if (activeField == txtfldFirstName2) {
            txtfldFirstName2.text = txtfldFirstName2.text?.capitalizingFirstLetter()
            if((txtfldFirstName2.text?.isEmpty)!) {
                imgViewStarFirstName2.image = UIImage(named: StarImages.starRed)
                imgViewStarFirstName2.isHidden = false
            }else{
                imgViewStarFirstName2.image = UIImage(named: StarImages.starYellow)
                imgViewStarFirstName2.isHidden = false
            }
        }
        else if (activeField == txtfldLastName1) {
            txtfldLastName1.text = txtfldLastName1.text?.capitalizingFirstLetter()
            if((txtfldLastName1.text?.isEmpty)!){
                imgViewStarLastName1.image = UIImage(named: StarImages.starRed)
                imgViewStarLastName1.isHidden = false
            }else{
                imgViewStarLastName1.image = UIImage(named: StarImages.starYellow)
                imgViewStarLastName1.isHidden = false
            }
        }
        else if (activeField == txtfldLastName2) {
            txtfldLastName2.text = txtfldLastName2.text?.capitalizingFirstLetter()
            if((txtfldLastName2.text?.isEmpty)!){
                imgViewStarLastName2.image = UIImage(named: StarImages.starRed)
                imgViewStarLastName2.isHidden = false
            }else{
                imgViewStarLastName2.image = UIImage(named: StarImages.starYellow)
                imgViewStarLastName2.isHidden = false
            }
        }
        
        
        if (activeField == txtfldFirstName) {
             txtfldFirstName.text = txtfldFirstName.text?.capitalizingFirstLetter()
            if((txtfldFirstName.text?.isEmpty)!) {
                imgViewStarFirstName.image = UIImage(named: StarImages.starRed)
                imgViewStarFirstName.isHidden = false
            }else{
                imgViewStarFirstName.image = UIImage(named: StarImages.starYellow)
                imgViewStarFirstName.isHidden = false
            }
        }else  if (activeField == txtfldLastName) {
            txtfldLastName.text = txtfldLastName.text?.capitalizingFirstLetter()
            if((txtfldLastName.text?.isEmpty)!){
                imgViewStarLastName.image = UIImage(named: StarImages.starRed)
                imgViewStarLastName.isHidden = false
            }else{
                imgViewStarLastName.image = UIImage(named: StarImages.starYellow)
                imgViewStarLastName.isHidden = false
            }
        }
        else  if (activeField == txtfldMobileNumber) {
            if(((txtfldMobileNumber.text?.isEmpty)!) || (!validateManager.isCountMobileNumber(phone: txtfldMobileNumber.text!)))
            {
                imgViewStarMobile.image = UIImage(named: StarImages.starRed)
                imgViewStarMobile.isHidden = false
            }else{
                imgViewStarMobile.image = UIImage(named: StarImages.starYellow)
                imgViewStarMobile.isHidden = false
            }
        }
        else  if (activeField == purposeAutoTextField) {
            
            purposeAutoTextField.text = purposeAutoTextField.text?.capitalizingFirstLetter()
            
            if((purposeAutoTextField.text?.isEmpty)!){
                imgViewStarPurpose.image = UIImage(named: StarImages.starRed)
                imgViewStarPurpose.isHidden = false
            }else{
                imgViewStarPurpose.image = UIImage(named: StarImages.starYellow)
                imgViewStarPurpose.isHidden = false
            }
        }else  if (activeField == txtfldCompanyName) {
            
            txtfldCompanyName.text = txtfldCompanyName.text?.capitalizingFirstLetter()
            
            if((txtfldCompanyName.text?.isEmpty)!){
                imgViewStarCompanyName.image = UIImage(named: StarImages.starRed)
                imgViewStarCompanyName.isHidden = false
            }else{
                imgViewStarCompanyName.image = UIImage(named: StarImages.starYellow)
                imgViewStarCompanyName.isHidden = false
            }
        }else  if (activeField == sponsorDropDown) {
            
             sponsorDropDown.text = sponsorDropDown.text?.capitalizingFirstLetter()
            
            if(((sponsorDropDown.text?.isEmpty)!) || (!validateManager.isValidEmail(testStr:getSelectedEmployeeEmailID))){
                imgViewStarSponsor.image = UIImage(named: StarImages.starRed)
                imgViewStarSponsor.isHidden = false
            }else{
                imgViewStarSponsor.image = UIImage(named: StarImages.starYellow)
                imgViewStarSponsor.isHidden = false
            }
        }else if !flagCheckImageChoose {
            //print("checkIsValidateTextFieldValue -- flagCheckImageChoose")
            guestImageView.layer.borderColor = UIColor.red.cgColor
            guestImageView.layer.borderWidth = 2.30
        }
    }
    func checkAllFieldsHasValue(){
        
        if(AppDelegate.appdelegateInstance().getSelectedLang == "Japanese"){
            if((txtfldFirstName1.text?.isEmpty)! || (txtfldLastName1.text?.isEmpty)! || (txtfldFirstName2.text?.isEmpty)! || (txtfldLastName2.text?.isEmpty)! || (txtfldMobileNumber.text?.isEmpty)! || (purposeAutoTextField.text?.isEmpty)! || (txtfldCompanyName.text?.isEmpty)! || (sponsorDropDown.text?.isEmpty)!){
                setSubmitColor(value: UIColor.gray,flag: false)
            }else{
                setSubmitColor(value: UIColor.white,flag: true)
            }
        }else{
            if((txtfldFirstName.text?.isEmpty)! || (txtfldLastName.text?.isEmpty)! || (txtfldMobileNumber.text?.isEmpty)! || (purposeAutoTextField.text?.isEmpty)! || (txtfldCompanyName.text?.isEmpty)! || (sponsorDropDown.text?.isEmpty)!){
                setSubmitColor(value: UIColor.gray,flag: false)
            }else{
                setSubmitColor(value: UIColor.white,flag: true)
            }
        }
       
        
        if (activeField == sponsorDropDown) {
            
            if(((sponsorDropDown.text?.isEmpty)!) || (!validateManager.isValidEmail(testStr:getSelectedEmployeeEmailID))){
                imgViewStarSponsor.image = UIImage(named: StarImages.starRed)
                imgViewStarSponsor.isHidden = false
            }else{
                imgViewStarSponsor.image = UIImage(named: StarImages.starYellow)
                imgViewStarSponsor.isHidden = false
            }
        }
    }
    func enableAndDisableStar(flagStar: Bool){
        imgViewStarFirstName.isHidden = flagStar
        imgViewStarLastName.isHidden = flagStar
        imgViewStarCompanyName.isHidden = flagStar
        imgViewStarMobile.isHidden = flagStar
        //imgViewStarEmail.isHidden = flagStar
        imgViewStarPurpose.isHidden = flagStar
        imgViewStarSponsor.isHidden = flagStar
        imgViewStarFirstName1.isHidden = flagStar
        imgViewStarLastName1.isHidden = flagStar
        imgViewStarFirstName2.isHidden = flagStar
        imgViewStarLastName2.isHidden = flagStar
        
    }
}
