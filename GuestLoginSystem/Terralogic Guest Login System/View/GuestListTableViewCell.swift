//
//  GuestListTableViewCell.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 10/10/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import UIKit

class GuestListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblText:UILabel!
    @IBOutlet weak var imgView:UIImageView!
    
    @IBOutlet weak var imgViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var imgViewConstraintWidht: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
