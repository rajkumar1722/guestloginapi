//
//  DateMangaer.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 20/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import Foundation

class DateManager{
    
    var dateTimer: Timer!
    
    func getTodayDate()->String{
        let formatter = DateFormatter()
        let now = Date()
        formatter.dateFormat = "MMM-dd-yyyy"
        let todayDate = formatter.string(from:now)
        return todayDate
    }
    
    func getCurrentDate()->String{
        let formatter = DateFormatter()
        let now = Date()
        formatter.dateFormat = "yyyy-MM-dd"
        let currentDate = formatter.string(from:now)
        return currentDate
    }
    func dateCheck(){
        
        let morningOfChristmasComponents = NSDateComponents()
        morningOfChristmasComponents.year = 2016
        morningOfChristmasComponents.month = 12
        morningOfChristmasComponents.day = 25
        morningOfChristmasComponents.hour = 7
        morningOfChristmasComponents.minute = 0
        morningOfChristmasComponents.second = 0
    }
 
    func tomorrowDate() -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.setValue(1, for: .day); // +1 day
        let now = Date() // Current date
        let tomorrow = Calendar.current.date(byAdding: dateComponents, to: now)  // Add the DateComponents
        return tomorrow!
    }
    
    func yesterdayDate() -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.setValue(-1, for: .day) // -1 day
        let now = Date() // Current date
        let yesterday = Calendar.current.date(byAdding: dateComponents, to: now) // Add the DateComponents
        return yesterday!
    }
    func calculateTrailVersion(){
        
        let objDateManager = DateManager()
        let getCurrentDate = Date()
        var getTrailCount: Int = 0
        
            if !(UserDefaults.standard.bool(forKey: "InstalledStatus"))
            {
                print("Set First time Installed Status")
                UserDefaults.standard.set( objDateManager.getCurrentDate(), forKey: "InstalledDate")
                UserDefaults.standard.set( getCurrentDate.currentDay, forKey: "InstalledDay")
                UserDefaults.standard.set( getCurrentDate.currentMonth, forKey: "InstalledMonth")
                UserDefaults.standard.set( getCurrentDate.currentYear, forKey: "InstalledYear")
                let getDeviceUUID = UUID()
                UserDefaults.standard.set( getDeviceUUID.uuidString, forKey: "Device_UDID")
                UserDefaults.standard.set( 1, forKey: "CountDay")
                UserDefaults.standard.set(false, forKey: "TrailVersionExpired")
                UserDefaults.standard.set(true, forKey: "InstalledStatus")
                UserDefaults.standard.synchronize()
                getTrailCount = 1
               
                
            }else if (UserDefaults.standard.bool(forKey: "InstalledStatus")){
                
                 print("Set Second time Installed Status")
                getTrailCount = UserDefaults.standard.integer(forKey: "CountDay")
                
                if (UserDefaults.standard.integer(forKey: "InstalledYear") == getCurrentDate.currentYear){
                    
                    if (UserDefaults.standard.integer(forKey: "InstalledMonth") == getCurrentDate.currentMonth){
                        
                        if (UserDefaults.standard.integer(forKey: "InstalledDay") == getCurrentDate.currentDay){
                            
                            UserDefaults.standard.set( getTrailCount, forKey: "CountDay")
                            UserDefaults.standard.synchronize()
                            
                        }else if (getCurrentDate.currentDay > UserDefaults.standard.integer(forKey: "InstalledDay")){
                            
                            if (getTrailCount <= 30){
                                UserDefaults.standard.set( getTrailCount+1, forKey: "CountDay")
                                UserDefaults.standard.synchronize()
                                
                            }else {
                                UserDefaults.standard.set(true, forKey: "TrailVersionExpired")
                                UserDefaults.standard.synchronize()
                            }
                        }
                        
                    }else if (getCurrentDate.currentMonth >= UserDefaults.standard.integer(forKey: "InstalledMonth")){
                        
                        if (getTrailCount <= 30){
                            UserDefaults.standard.set( getTrailCount+1, forKey: "CountDay")
                            UserDefaults.standard.synchronize()
                        }else {
                            UserDefaults.standard.set(true, forKey: "TrailVersionExpired")
                            UserDefaults.standard.synchronize()
                        }
                    }
                }else if (getCurrentDate.currentYear >=  UserDefaults.standard.integer(forKey: "InstalledYear")){
                    if (getCurrentDate.currentMonth >= UserDefaults.standard.integer(forKey: "InstalledMonth")){
                        if (getCurrentDate.currentDay >= UserDefaults.standard.integer(forKey: "InstalledDay")){
                            UserDefaults.standard.set(true, forKey: "TrailVersionExpired")
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
            }
        }
}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    
    var currentDay: Int {
        return Calendar.current.component(.day,  from: self)
    }
    
    var currentMonth: Int {
        return Calendar.current.component(.month,  from: self)
    }
    
    var currentYear: Int {
         return Calendar.current.component(.year,  from: self)
    }
    
    func years(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.year], from: sinceDate, to: self).year
    }
    
    func months(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.month], from: sinceDate, to: self).month
    }
    
    func days(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.day], from: sinceDate, to: self).day
    }
    
    func hours(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.hour], from: sinceDate, to: self).hour
    }
    
    func minutes(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.minute], from: sinceDate, to: self).minute
    }
    
    func seconds(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.second], from: sinceDate, to: self).second
    }
}
