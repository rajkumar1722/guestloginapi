//
//  ValidationManager.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 04/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import UIKit

class ValidationManager: NSObject {

    //MARK: - Email Validation :
    func isValidEmail(testStr:String) -> Bool {
        let regexEmail = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", regexEmail)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    func isvalidMobileNumber(phone: String) -> Bool {
        let phoneRegex = "^((0091)|(\\+91)|0?)[6789]{1}\\d{9}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }
    func isCountMobileNumber(phone:String)->Bool{
        if phone.count == 10{
            return true
        }
        else{
            return false
        }
    }
    //MARK: - Password Validation : Check current and Confirm is Same.
    func isPasswordSame(password: String , confirmPassword : String) -> Bool {
        if password == confirmPassword{
            return true
        }else{
            return false
        }
    }
    // MARK - Password length Validation - Length should grater than 7.
    func isPwdLenth(password: String , confirmPassword : String) -> Bool {
        if password.count <= 7 && confirmPassword.count <= 7{
            return true
        }else{
            return false
        }
    }
    //MARK: - Pincode Validation :
    func isValidPincode(value: String) -> Bool {
        if value.count == 6{
            return true
        }
        else{
            return false
        }
    }
}
