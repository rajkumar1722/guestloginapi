//
//  GuestDetailsModel.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 06/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import Foundation

struct employeeList : Codable {
    
    var firstName: String
    var lastName: String
    var userId: String
    var projectName: String
    var employeeEmailID: String
    var jobTitle: String
    var groupName: String
}

class GuestDetailsModel
{
    var guestFirstName: String?
    var comingFrom: String?
    var mobileNumber: String?
    var guestEmailId: String?
    var govtTaxID: String?
    var purpose: String?
    var todayDate: String?
    var empMailID: String?
    var guestImage: String?
    var guestID: String?
    var guestLastName: String?
    var companyName: String?
    var sponsorName: String?
    var guestName: String?
    var guestImageName: String?
    var projectName: String?
    
    func clearAllData(){
        
        guestFirstName = nil
        guestLastName = nil
        companyName = nil
        sponsorName = nil
        guestName = nil
        comingFrom = nil
        mobileNumber = nil
        guestEmailId = nil
        govtTaxID = nil
        purpose = nil
        todayDate = nil
        empMailID = nil
        guestImage = nil
        guestID = nil
        guestImageName = nil
        projectName = nil
    }
}
