//
//  FileManagers.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 03/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import UIKit

enum enumEmployeeList{
    
    case Vietnam
    case India
    case Customer
}
class FileManagers: NSObject {

    var employeeArray:[Dictionary<String, AnyObject>] =  Array()
    
    // MARK: - Create dictionary from all guest records in core data
    func createDictionaryFormat(arrGetGuestList: Array<Any>,completion:@escaping((_ flagStatus: Bool) -> Void)){
          //print("createDictionaryFormat")
        var rowcount = 1
        for value in arrGetGuestList{
            let guestData:(Guest) = value as! (Guest)
            
            var dct = Dictionary<String, AnyObject>()
            
            dct.updateValue(rowcount as AnyObject, forKey: "ID")
            dct.updateValue(guestData.guestName as AnyObject, forKey: "Name")
            dct.updateValue(guestData.comingFrom as AnyObject, forKey: "Comingfrom")
            dct.updateValue(guestData.mobileNumber as AnyObject, forKey: "MobileNumber")
            dct.updateValue(guestData.guestEmailId as AnyObject, forKey: "EmailId")
            dct.updateValue(guestData.govtTaxID as AnyObject, forKey: "Govt.ProofId")
            dct.updateValue(guestData.purpose as AnyObject, forKey: "Purpose")
            dct.updateValue(guestData.empMailID as AnyObject, forKey: "EmpEmailID")
            dct.updateValue(guestData.guestID as AnyObject, forKey: "GuestID")
           // dct.updateValue(guestData.guestImage as AnyObject, forKey: "ImageString")

            ///print("check dictionary = \(dct)")
            rowcount = rowcount + 1
            employeeArray.append(dct)
        }
         createExcelSheet(from: employeeArray)
         completion(true)
    }
    // MARK: - Creating Excel file for all records in coredata
    func createExcelSheet(from recArray:[Dictionary<String, AnyObject>]) {

        //print("createExcelSheet")
        
        let txtID = NSLocalizedString("Id", comment: "")
        let txtName = NSLocalizedString("Name", comment: "")
        let txtLocation = NSLocalizedString("Location", comment: "")
        let txtMobileNumber = NSLocalizedString("Mobile Number", comment: "")
        let txtGuestEmailId = NSLocalizedString("Guest Email Id", comment: "")
        let txtGovtProofId = NSLocalizedString("Govt. Proof Id", comment: "")
        let txtPurpose = NSLocalizedString("Purpose", comment: "")
        let txtEmailID = NSLocalizedString("Email ID", comment: "")
        let txtDeviceType = NSLocalizedString("Device Type", comment: "")
        
        var csvString = "\(txtID) , \(txtName) , \(txtLocation) , \(txtMobileNumber) , \(txtGuestEmailId) , \(txtGovtProofId) , \(txtPurpose) , \(txtEmailID) , \(txtDeviceType)\n\n"
        
        for dct in recArray{
            csvString = csvString.appending(" \(String(describing: dct["ID"]!)) ,\(String(describing: dct["Name"]!)) , \(String(describing: dct["Comingfrom"]!)) , \(String(describing: dct["MobileNumber"]!)) , \(String(describing: dct["EmailId"]!)) , \(String(describing: dct["Govt.ProofId"]!)) , \(String(describing: dct["Purpose"]!)) , \(String(describing: dct["EmpEmailID"]!)) , \(String(describing: dct["GuestID"]!)),\n")
            // \(String(describing: dct["ImageString"]!)
        }
        
        let fileManager = FileManager.default
        do {
            let path = try fileManager.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
            let fileURL = path.appendingPathComponent("GuestsRecords.xlsx")
            //print("fileURL \(fileURL)")
            try csvString.write(to: fileURL, atomically: true, encoding: .utf8)
        } catch {
            print("error creating file")
        }
        
    }
    
    //MARK: - Image Convert to base64
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg( _): imageData = image.jpegData(compressionQuality: 8.0)
        }
        return imageData?.base64EncodedString()
    }
    /*func getImageFromBase64(base64:String) -> UIImage {
        let data = Data(base64Encoded: base64)
        return UIImage(data: data!)!
    }
    class func convertImageToBase64String(image:UIImage)->String{
        let imageData:NSData = image.pngData()! as NSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64
    }
    //MARK: - Image Convert to base64
    class func convertBase6StringToImage(strBase64: String)-> UIImage{
        let dataDecoded: Data = Data(base64Encoded: strBase64, options: .ignoreUnknownCharacters)!
        let decodedimage: UIImage = UIImage(data: dataDecoded)!
        return decodedimage
    }*/
    
    //MARK: - Image Convert to base64
    class func getFileLocation()->URL{
        let fileManager = FileManager.default
        var fileURL: URL = URL(fileURLWithPath:"")
        do {
            let path = try fileManager.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
            fileURL = path.appendingPathComponent("GuestsRecords.xlsx")
             return fileURL
        } catch {
            print("error creating file")
        }
        return fileURL
    }
    //MARK: - Read all read employee details form json file in local resource folder -- Old List
    class func getAllEmployeeDetails(strName:enumEmployeeList){
        
        var fileName = ""
        switch strName{
        case .India:
             fileName = "TerralogicEmployeesList"
        case .Vietnam:
            fileName = "VietnamEmployeesList"
        case .Customer:
            fileName = "CustomerFileName"
        }
        
        if let path = Bundle.main.path(forResource:fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                for valueEmpDet in jsonResult as! [[String:Any]] {
                    if let empFirstName = valueEmpDet["First Name"] , let empLastName = valueEmpDet["Last Name"] , let emailId = valueEmpDet["Email Address"]
                    {
                        let newStringFirstName = (empFirstName as! String).replacingOccurrences(of: ".", with: " ")
                        let newStringLastName = (empLastName as! String).replacingOccurrences(of: ".", with: " ")
                        
                        let arrEmpName = ["firstName": "\(newStringFirstName)", "lastName": "\(newStringLastName)", "employeeEmailID": "\(emailId as! String)"]
                        AppDelegate.appdelegateInstance().arrGetEmployeeNameList.append(arrEmpName)
                    }
                }
                let nc = NotificationCenter.default
                nc.post(name: Notification.Name("updateEmailList"), object: nil)
                
            } catch {
                // handle error
            }
        }
    }
    //MARK: - Read all read employee details form json file in local resource folder -- Old List
    class func readAllEmployeeDetails(){
        if let path = Bundle.main.path(forResource: "EmployeeEmailIdList", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                for valueEmpDet in jsonResult as! [Any] {
                   
                    let employeeEmailID = valueEmpDet as! [String:Any]
                    
                    if let emailId = employeeEmailID["Company Email"] {
                        //AppDelegate.appdelegateInstance().getEmployeeEmailIdList.append(emailId as! String)
                        
                        if let empFirstName = employeeEmailID["First Name"], let empLastName = employeeEmailID["Last Name"]{
                            
                           // let newStringFirstName = (empFirstName as! String).replacingOccurrences(of: ".", with: " ")
                           // let newStringLastName = (empLastName as! String).replacingOccurrences(of: ".", with: " ")
                            
                            let arrEmpName = ["FirstName": "\(empFirstName)", "LastName": "\(empLastName)", "EmployeeEmailID": "\(emailId as! String)"]
                            //AppDelegate.appdelegateInstance().getEmployeeNameList.append(empName as String)
                            AppDelegate.appdelegateInstance().arrGetEmployeeNameList.append(arrEmpName)
                        }
                    }
                }
                //print("FullName = \(AppDelegate.appdelegateInstance().arrGetEmployeeNameList)")
            } catch {
                // handle error
            }
        }
    }
    //MARK: - File handler Function
    func checkFileExistOrNot(fileName: String)->Bool{
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("GuestPhotos")?.appendingPathComponent(fileName) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                //print("FILE AVAILABLE")
                return true
            } else {
                //print("FILE NOT AVAILABLE")
                return false
            }
        } else {
            //print("FILE PATH NOT AVAILABLE")
            return true
        }
    }
    func deleteDirectoryFromHomeDirectory() {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("GuestPhotos") {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                //print("FOLDER AVAILABLE")
                 try? fileManager.removeItem(atPath: filePath)
            } else {
                //print("FOLDER NOT AVAILABLE")
            }
        } else {
            //print("FOLDER PATH NOT AVAILABLE")
        }
    }
    func deleteFileFromDirectory(fileName: String){
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("GuestPhotos")?.appendingPathComponent(fileName) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                //print("FILE AVAILABLE Deleted")
                try? fileManager.removeItem(atPath: filePath)
            } else {
                //print("FILE NOT AVAILABLE")
             }
        } else {
            //print("FILE PATH NOT AVAILABLE")
         }
    }

    public static var documentsDirectoryURL: URL {
        return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
    }
    public static func fileURLInDocumentDirectory(_ fileName: String) -> URL {
        return self.documentsDirectoryURL.appendingPathComponent(fileName)
    }
    
    class func createFolder(){
        let fileManager = FileManager.default
        if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            let filePath =  tDocumentDirectory.appendingPathComponent("GuestPhotos")
            if !fileManager.fileExists(atPath: filePath.path) {
                do {
                    try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                    //print("create document directory")
                } catch {
                    //print("Couldn't create document directory")
                }
            }
            //print("Document directory is \(filePath)")
        }
    }
    class func checkFolderCreatedOrNot()->Bool{
        let fileManager = FileManager.default
        if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            let filePath =  tDocumentDirectory.appendingPathComponent("GuestPhotos")
            if !fileManager.fileExists(atPath: filePath.path) {
                return true
            }else {
                return false
            }
        }
        return false
    }
    func saveImage(image: UIImage, personName: String) -> Bool {
        //print("--- personName = \(personName)------")
        
        guard image.jpegData(compressionQuality: 8.0) != nil else{
            return false
        }
        guard let dataPNG = image.pngData() else {
             return false
        }
        
        //UIImageJPEGRepresentation(image, 8.0) ?? UIImagePNGRepresentation(image) else {
//        guard let data = image.jpegData(compressionQuality: 8.0) else {
//            return false
//        }
    
        
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            //print("personName = \(personName)")
            let filePath = directory.appendingPathComponent("GuestPhotos")?.appendingPathComponent("\(personName)")
             try dataPNG.write(to: filePath!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    func getSavedImage(named: String) -> URL? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return  URL(fileURLWithPath: dir.absoluteString).appendingPathComponent("GuestPhotos").appendingPathComponent("\(named).png")
        }
        return nil
    }
    
    func getSavedImagePath(named: String) -> String {
        
        let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL
        let filePath = directory?.appendingPathComponent("GuestPhotos")?.appendingPathComponent("\(named)")
        //print("File Location = \(String(describing: filePath))")  //.appendingPathComponent("\(named)" as string))
        return (filePath?.path)!
    }
}
