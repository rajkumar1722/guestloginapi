//
//  ApiManager.swift
//  Terralogic Guest Login System
//
//  Created by Rajkumar on 12/07/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import Foundation
import UIKit

//1: Upload

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
extension NSMutableData {
    
    func appendStrings(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

class ApiManager{
    
    class func imageUpload(parameters: [String:Any], imagePath: String){
        
        let boundary = "Boundary-\(UUID().uuidString)"
        let boundaryPrefix = "--\(boundary)\r\n"
        
        let headers = [
            "content-type": "multipart/form-data; boundary=\(boundary)",
            "Authorization": "bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca",
        ]
        
        let url = URL(fileURLWithPath: imagePath)
        let filename = url.lastPathComponent
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.239.166.141:8888/api/file/")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 260.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers

        let body = NSMutableData()
        let mimeType = "image/png"

        for (key, value) in parameters {
            
            print("Check boundary Key = \(key), Value = \(value)")
                
            body.appendStrings(boundaryPrefix)
//            body.appendStrings("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//            body.appendStrings("\(value)\r\n")
            
            //if key == "image"{
                body.appendStrings("Content-Disposition: form-data; file=\"\(filename)\"\r\n")
                body.appendStrings("Content-Type: \(mimeType)\r\n\r\n")
                
                if let getImageData = NSData(contentsOfFile: imagePath) {
                    body.append(getImageData as Data)
                }
           // }else{
           //     body.appendStrings("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
           //     body.appendStrings("\(value)\r\n")
            //}
        }
        
        body.appendStrings("\r\n")
        body.appendStrings("--".appending(boundary.appending("--")))
        request.httpBody = body as Data
        
        let session = URLSession.shared
        session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            }.resume()
    }
    class func requestImageTestUpload(params: [String:Any], imagePath: String) {
        
        
        let headers = [
            "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            "Authorization": "bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca",
            "Content-Type": "multipart/form-data",
        ]
        
        let url = URL(fileURLWithPath: imagePath)
        let filename = url.lastPathComponent
        
        //let parameters = ["name": "MyTestFile123321", "description": "My tutorial test file for MPFD uploads"]
        
        let parameters = [["name": "image","fileName": "\(filename)","content-type": "image/png"]]
        
        //guard let mediaImage = Media(withImage: #imageLiteral(resourceName: "testImage"), forKey: "image") else { return }
        
       // guard let url = URL(string: "https://api.imgur.com/3/image") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let boundary = generateBoundary()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue("Client-ID f65203f7020dddc", forHTTPHeaderField: "Authorization")
        
        var getImageData: NSData? = nil
        if let cert = NSData(contentsOfFile: imagePath) {
            getImageData = cert
        }
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            }.resume()
    }
    class func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createDataBody(parameters: [String: String], boundary: String, data: Data, mimeType: String,filename: String) -> Data {
        
        let body = NSMutableData()
            let boundaryPrefix = "--\(boundary)\r\n"
            
            for (key, value) in parameters {
                body.appendString(boundaryPrefix)
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
            
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimeType)\r\n\r\n")
            body.append(data)
            body.appendString("\r\n")
            body.appendString("--".appending(boundary.appending("--")))
            
            return body as Data
    }
    class func requestImageUpload(params: [String:Any], imagePath: String){
        
        let headers = [
            "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            "Authorization": "bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca",
            "Content-Type": "multipart/form-data",
        ]
        
        let url = URL(fileURLWithPath: imagePath)
        let filename = url.lastPathComponent
        
        let parameters = [["name": "image","fileName": "\(filename)","content-type": "image/png"]]
        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        var body = ""
        var error: NSError? = nil
        for param in parameters {
            let paramName = param["name"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if let filename = param["fileName"] {
                
                let contentType = param["content-type"]!
                
                var getImageData: NSData? = nil
                if let cert = NSData(contentsOfFile: imagePath) {
                    getImageData = cert
                }
                
                /*var fileContent:String = ""
                
                do {
                    fileContent = try String(contentsOfFile: filename, encoding: String.Encoding.utf8)
                } catch let error1 as NSError {
                    error = error1

                } catch {
                    // Catch any other errors
                }*/
                
                
                if (error != nil) {
                    print(error!)
                }
                body += "; filename=\"\(filename)\"\r\n"
                body += "Content-Type: \(contentType)\r\n\r\n"
                //body += getImageData!
            } else if let paramValue = param["value"] {
                body += "\r\n\r\n\(paramValue)"
            }
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.239.166.141:8888/api/file/")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        
        request.httpBody = Data(base64Encoded: body)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse ?? nil!)
            }
        })
        
        dataTask.resume()
        /*
        let headers = [
            "Authorization": "bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca",
            "Content-Type": "multipart/form-data",
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.239.166.141:8888/api/file/")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        
        let jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        request.httpBody = jsonData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            guard let dataResponse = data, let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            let responseString = String(data: dataResponse, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            do{
                let jsonResponse = try JSONSerialization.jsonObject(with:dataResponse, options: .allowFragments)
                print(jsonResponse)
            } catch let parsingError {
                print("Error", parsingError)
            }
        })
        dataTask.resume()*/
    }
    
    class func requestGetGuestMeetByMeetCode(strUrl: String){
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca",
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: strUrl)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let dataResponse = data, let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            let responseString = String(data: dataResponse, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            do{
                let jsonResponse = try JSONSerialization.jsonObject(with:dataResponse, options: .allowFragments)
                print(jsonResponse)
            } catch let parsingError {
                print("Error", parsingError)
            }
        })
        
        dataTask.resume()
    }
    
    class func CreateANewMeetGuest()  {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca",
        ]
        let parameters = [
            "firstName": "Thomas",
            "lastName": "Nguyen ",
            "avatar": "https://png.pngtree.com/element_our/sm/20180327/sm_5aba147bcacf2.png",
            "contactNumber": "+8409999999",
            "email": "thomasnguyendev@gmail.com",
            "companyName": "terralogic.com",
            "meetPeople": "855",
            "meetPurpose": "Go to meeting"
            ] as [String : Any]
        
        _ = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.239.166.141:8888/api/guest")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        
       //request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error ?? nil!)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse ?? nil!)
            }
        })
        
        dataTask.resume()

    }
}
