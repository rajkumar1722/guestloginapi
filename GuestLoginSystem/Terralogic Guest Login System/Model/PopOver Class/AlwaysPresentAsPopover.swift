//
//  AlwaysPresentAsPopover.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 21/05/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import UIKit

enum Direction : String {
    case English, Japanese, Vietnamese
    static var allValues = [Direction.English, .Japanese, .Vietnamese]
    
}

struct ExampleModel {
    var direction = Direction.English
}

class AlwaysPresentAsPopover: NSObject, UIPopoverPresentationControllerDelegate {

    // `sharedInstance` because the delegate property is weak - the delegate instance needs to be retained.
    private static let sharedInstance = AlwaysPresentAsPopover()
    
    private override init() {
        super.init()
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    static func configurePresentation(forController controller : UIViewController) -> UIPopoverPresentationController {
        controller.modalPresentationStyle = .popover
        let presentationController = controller.presentationController as! UIPopoverPresentationController
        presentationController.delegate = AlwaysPresentAsPopover.sharedInstance
        return presentationController
    }
}
