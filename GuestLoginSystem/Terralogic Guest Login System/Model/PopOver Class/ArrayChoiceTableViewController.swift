//
//  ArrayChoiceTableViewController.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 21/05/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import Foundation
import UIKit

class ArrayChoiceTableViewController<Element> : UITableViewController {
    
    typealias SelectionHandler = (Element) -> Void
    typealias LabelProvider = (Element) -> String
    
    private let values : [Element]
    private let labels : LabelProvider
    private let onSelect : SelectionHandler?
    
    var getSelectedLang: String? {
        get {
            let value = UserDefaults.standard.string(forKey:"flag_key_Language")
            return value
        }
        set {
            AppDelegate.appdelegateInstance().getSelectedLang = newValue
            UserDefaults.standard.set(newValue, forKey: "flag_key_Language")
            UserDefaults.standard.synchronize()
        }
    }
    
    init(_ values : [Element], labels : @escaping LabelProvider = String.init(describing:), onSelect : SelectionHandler? = nil) {
        self.values = values
        self.onSelect = onSelect
        self.labels = labels
        super.init(style: .plain)
        self.tableView.backgroundColor = UIColor.lightText
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = labels(values[indexPath.row])
        
        let selectedValue = labels(values[indexPath.row])
        if selectedValue == getSelectedLang {
            cell.accessoryType = .checkmark
        }else {
            cell.accessoryType = .none
        }
       
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        self.tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        self.dismiss(animated: true)
        onSelect?(values[indexPath.row])
        //print("Selected = \(values[indexPath.row])")
        guard let selectedValue = values[indexPath.row] as? Direction else  {
            print("Failed to get the Selected LAng")
            return
        }
        
       //print("selectedValue = \(selectedValue)")
        switch selectedValue {
        case Direction.Vietnamese:
                Bundle.set(language: Language.vietnamese)
                //L102Language.setAppleLanguage(lang: Language.vietnam)
                getSelectedLang = "Vietnamese"
            case Direction.Japanese:
                Bundle.set(language: Language.japanese)
                //L102Language.setAppleLanguage(lang: Language.japanese)
                getSelectedLang = "Japanese"
            case Direction.English:
                Bundle.set(language: Language.english(.us))
                //L102Language.setAppleLanguage(lang: Language.english(.us))
                getSelectedLang = "English"
        }
        //print("getSelectedLang = \(String(describing: getSelectedLang))")
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name("languageReload"), object: nil)
    }
}

