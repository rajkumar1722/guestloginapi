//
//  AppVersionModel.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 09/07/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import Foundation

struct AppVersion: Codable {
    let serverTimestamp: Int
    let appVersions: String
}
