//
//  DropDownData.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 04/10/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import Foundation
public class Options {
    let boolData = ["true","false"]
    let color = ["blue","brown","clear","cyan","gray","green","lightGray","orange","purple","red","white","yellow","black"]
    let height = ["50","80","100","150","200"]
    
    let purposeData = ["Client Visit","Interview","Banking","Delivery","Maintanence"]
    
    let emailIData = ["swami@terralogic.com", "laiju.gangadharan@terralogic.com","satya@terralogic.com", "rajeeve@terralogic.com","rajkumar.m@terralogic.com", "nitin.kr@terralogic.com"]
}
