//
//  LocaleManager.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 21/05/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import Foundation
import UIKit

struct LocaleManager {
    
    /// "ko-US" → "ko"
    static var languageCode: String? {
        guard var splits = Locale.preferredLanguages.first?.split(separator: "-"), let first = splits.first else { return nil }
        guard 1 < splits.count else { return String(first) }
        splits.removeLast()
        return String(splits.joined(separator: "-"))
    }
    
    static var language: Language? {
        return Language(languageCode: languageCode)
    }
}

extension String {
    func localized(_ lang:String) ->String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}


let APPLE_LANGUAGE_KEY = "AppleLanguages"


/// L102Language
class L102Language {
    /// get current Apple language
    class func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    /// set @lang to be the first in Applelanguages list
    class func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
    
    // Setting the Language in Apple
    class func setAppleLanguage(lang: Language){
        switch lang {
        case Language.japanese:
             L102Language.setAppleLAnguageTo(lang: "ja-JP")
        case Language.vietnamese:
             L102Language.setAppleLAnguageTo(lang: "vi-VN")
        default:
             L102Language.setAppleLAnguageTo(lang: "en")
        }
    }
}
