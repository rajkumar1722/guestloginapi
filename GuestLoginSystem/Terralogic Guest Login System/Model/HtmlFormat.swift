//
//  HtmlFormat.swift
//  CustomPrinter
//
//  Created by Satya-MINI on 27/12/18.
//  Copyright © 2018 Satya-MINI. All rights reserved.
//

import UIKit

class HtmlFormat: NSObject {

    let strHTMLCodeForCard = ""
    class func createVisitorCard(visitorName:String,visitorCompanyName:String,visitorImage:String, imageLogo:String, meetingPersonName:String)->String!
    {
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        let dateString = formatter.string(from: Date())
        Swift.print(dateString)   // "4:44 PM on June 23, 2016\n"
        
        let imageLogoHMTL = "<tr style=height:50px;><th colspan=2><img src=\"data:image/jpeg;base64,\(imageLogo)\", style= width:200px; height:50px;><hr></th></tr>"
        let imageHtmlCode = "<td style=width:140px;><img src=\"data:image/jpeg;base64,\(visitorImage)\",  style = width:130px; height: 130px; margin-left: 5px; border-radius: 50%; alt=Red dot></td>"
        
        let setHTMLFormat = """
        <!DOCTYPE html><html><meta http-equiv=Content-Type content=text/html; charset=UTF-8><body><div style=" width:480px; height: 300px;"><table class=no-spacing cellspacing=0 style="width:480px; height: 300px; background-color: clear;">\(imageLogoHMTL)<tr style="background-color: clear; height: auto;">\(imageHtmlCode)<td style="padding-left:4%; background-color: clear;"><h2 style="color: black; font-family: Helvetica; font-size: 20px; margin-left:0px;">\(visitorName)</h2><h3 style="color: black; font-family: Helvetica; font-size: 16px; margin-left: 0px;">\(visitorCompanyName)</h3><h3 style="color: black; font-family: Helvetica; font-size: 16px; margin-left: 0px;">Checkin Time : \(dateString)</h3><h3 style="color: black; font-family: Helvetica; font-size: 16px; margin-left: 0px; padding-bottom: 15px;">\(meetingPersonName)</h3><td></td></td></tr></table></div></body></html>
        """
        return setHTMLFormat
    }

}
