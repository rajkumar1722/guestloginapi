//
//  MFCameraError.swift
//  
//
//  Created by Mohammad Ali Jafarian on 21/2/18.
//

import Foundation

enum MFCameraError: Error {
    case noVideoConnection
    case noImageCapture
    case crop
    case noMetaRect
    case noDevice
}

extension MFCameraError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noVideoConnection:
            return NSLocalizedString("there is no video connection", comment: "")
        case .noImageCapture:
            return NSLocalizedString("could not capture any image", comment: "")
        case .crop:
            return NSLocalizedString("error occured during image crop", comment: "")
        case .noMetaRect:
            return NSLocalizedString("no metadata rect found", comment: "")
        case .noDevice:
            return NSLocalizedString("your device doesnt have camera", comment: "")
        }
    }
}
