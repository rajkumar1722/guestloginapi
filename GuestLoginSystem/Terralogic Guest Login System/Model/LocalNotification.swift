//
//  LocalNotification.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 29/08/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import UIKit
import UserNotifications
class LocalNotification: NSObject {

    func setDailyLocalNotification(){
        
        let notification = UNMutableNotificationContent()
        notification.title = NSLocalizedString("Email Candidate Reports", comment: "")
        //notification.subtitle = ""
        notification.body = NSLocalizedString("Proceed here to send all records to operation team.", comment: "")
        notification.sound = UNNotificationSound.default
        // add notification for Mondays at 11:00 p.m.
        var dateComponents = DateComponents()
        //dateComponents.weekday = 6
        dateComponents.hour = 22
        dateComponents.minute = 20
        let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let request = UNNotificationRequest(identifier: "notification1", content: notification, trigger: notificationTrigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
}
