//
//  WSHelper.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 02/01/19.
//  Copyright © 2019 Terralogic. All rights reserved.
//

import UIKit

protocol WSHelpherProtocol {
    func getAppVersion(getAppversion:AppVersion)
}
extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
extension Data {
    
    /// Append string to Data
    ///
    /// Rather than littering my code with calls to `data(using: .utf8)` to convert `String` values to `Data`, this wraps it in a nice convenient little extension to Data. This defaults to converting using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `Data`.
    
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
class WSHelper: NSObject {

    var WSHelpherProtocol: WSHelpherProtocol?
    
    class func funAPIRequestMethod(typeMethod:String, getUrlLink: String, completionHandler: @escaping (_ result: [String: Any]?, _ error: Error?) -> Void) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca",
        ]
        
        let request = NSMutableURLRequest(url:NSURL(string: getUrlLink)! as URL,
                    cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = typeMethod
        request.allHTTPHeaderFields = headers
    
        // let url = URL(string: getUrlLink)!
        // var request = URLRequest(url: url)
        //request.httpMethod = typeMethod
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.addValue("bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca", forHTTPHeaderField: "Authorization")

        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let dataResponse = data, let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            let responseString = String(data: dataResponse, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:dataResponse, options: .allowFragments)
                print(jsonResponse) //Response result
                //let decoder = JSONDecoder()
               
//                let jsonResponseModel = try decoder.decode(AppVersion.self, from:dataResponse) //Decode JSON Response Data
//                print(jsonResponseModel)

                completionHandler(jsonResponse as? [String : Any], error)
                
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
    
    class func requestPostMethod(getUrl:String, parameters:[String: Any] ,completion: @escaping ([String: Any]?, Error?) -> Void){
        
        print("requestPostMethod = \(getUrl)")
        let url = URL(string: getUrl)!
        var request = URLRequest(url: url)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
      
        print("requestPostMethod parameters = \(String(describing: parameters))")
        let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            //completion(responseString, error)
        }
        
        task.resume()
    }
    /// Create request
    ///
    /// - parameter userid:   The userid to be passed to web service
    /// - parameter password: The password to be passed to web service
    /// - parameter email:    The email address to be passed to web service
    ///
    /// - returns:            The `URLRequest` that was created
    //MARK:--
    func funRequestPostImageUpload(uploadLink:String,imageName:String, parameters: [String : Any]){
        let request: URLRequest
        do {
            request = try createRequest(imageUploadLink:uploadLink,getImageName: imageName, getParameters: parameters)
        } catch {
            print(error)
            return
        }
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
        }
        task.resume()
    }
    func createRequest(imageUploadLink:String,getImageName:String, getParameters: [String : Any]) throws -> URLRequest {
        
        let boundary = generateBoundaryString()
        let url = URL(string: imageUploadLink)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca", forHTTPHeaderField: "Authorization")
        let path1 = FileManagers().getSavedImagePath(named: getImageName)
        request.httpBody = try createBody(with: getParameters as? [String: String], filePathKey: "file", paths: path1, boundary: boundary)
        
        return request
    }
    
    /// Create body of the `multipart/form-data` request
    ///
    /// - parameter parameters:   The optional dictionary containing keys and values to be passed to web service
    /// - parameter filePathKey:  The optional field name to be used when uploading files. If you supply paths, you must supply filePathKey, too.
    /// - parameter paths:        The optional array of file paths of the files to be uploaded
    /// - parameter boundary:     The `multipart/form-data` boundary
    ///
    /// - returns:                The `Data` of the body of the request
    
    private func createBody(with parameters: [String: String]?, filePathKey: String, paths: String, boundary: String) throws -> Data {
        var body = Data()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        //for path in paths {
            let url = URL(fileURLWithPath: paths)
            let filename = url.lastPathComponent
            let data = try Data(contentsOf: url)
            let mimetype = "image/png"
            
            body.append("--\(boundary)\r\n")
            body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
            body.append("Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.append("\r\n")
        //}
        
        body.append("--\(boundary)--\r\n")
        
        return body
    }
    
    /// Create boundary string for multipart/form-data request
    ///
    /// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
    
    private func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    class func requestImageUploadPostMethod(uploadLink:String,imageName:String, parameters: [String : Any]){
        
        print("uploadLink = \(uploadLink)")
       
        let headers = [
            "Authorization": "bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca",
            "Content-Type": "multipart/form-data",
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.239.166.141:8888/api/file/")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
         request.allHTTPHeaderFields = headers
        
        request.httpMethod = "POST"
        let boundary = "Boundary-\(UUID().uuidString)"
        let path1 = FileManagers().getSavedImagePath(named: imageName)
        let url = URL(fileURLWithPath: path1)
        let filename = url.lastPathComponent
        
        print("filename = \(filename), imageName = \(imageName)")
        
            do{
            //here dataResponse received from a network request
            let imageData = try Data(contentsOf: url)
            request.httpBody = createBodyImageUploadTest(parametersPost: parameters,boundary: boundary, data:imageData,mimeType: "image/png",filename: imageName)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard let data = data,
                    let response = response as? HTTPURLResponse,
                    error == nil else {                                              // check for fundamental networking error
                        print("error", error ?? "Unknown error")
                        return
                }
                guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                    print("statusCode should be 2xx, but is \(response.statusCode)")
                    print("response = \(response)")
                    return
                }
                
                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(String(describing: responseString))")
            }
          task.resume()
        } catch let parsingError {
            print("Error --- imageData = ", parsingError)
        }
    }
    class func createBodyImageUploadTest(parametersPost: [String: Any],boundary: String, data: Data,mimeType: String,filename: String) -> Data {
        
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parametersPost {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        
        return body as Data
    }
    //MARK:--
    class func requestPostMethods(limitValue: Int){
        
        let json: [String: Any] = ["location": "INDIA","start":0,"limit":limitValue]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: "https://intranet.terralogic.com/public/api/getemployeesbylocation")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //request.setValue("bearer 051aa63f2de941b4bb905b1ff2ac645002bfe3c669d840bc31c014ac5be33fca", forHTTPHeaderField: "Authorization")
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let responseData = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: responseData, options: [])
            let responseResult = responseJSON as? [String:Any]
            guard let jsonArray = responseResult?["data"] as? [String:Any] else {
                return
            }
            //print("jsonArray = \(jsonArray)")
            let filteredTotal = jsonArray.filter { $0.0 == "total" }
            //print("filtered total = \(String(describing: filteredTotal["total"]))")
            let totalValue = filteredTotal["total"] as! Int
            //print("totalValue = \(String(describing: totalValue))")
            if(limitValue == 1){
                DispatchQueue.main.async {
                    // Perform your async code here
                    //self.requestPostMethod(limitValue: totalValue)
                }
            }else{
                
                let filteredUser = jsonArray.filter { $0.0 == "users" }
                for userList in (filteredUser["users"] as? [[String:Any]])!
                {
                    let projectDict = userList["project"] as? [String:Any]
                    if let empFirstName = userList["userName"], let emailId = userList["companyEmail"], let userID = userList["userId"], let groupName = userList["groupName"], let jobTitle = userList["jobTitle"], let projectName = projectDict?["projectName"]{
                        
                        //print("empFirstName = \(empFirstName)")
                        var myStringArr = (empFirstName as! String).components(separatedBy: " ")
                        //print("myStringArr.count = \(myStringArr.count)")
                        let strFirstName = myStringArr[0]
                        var strSecondName = ""
                        
                        if (myStringArr.count>6){
                            strSecondName = myStringArr[1] + " " + myStringArr[2] + " " + myStringArr[3] + " " + myStringArr[4] + " " + myStringArr[5] + " " + myStringArr[6]
                        }
                        else if (myStringArr.count>5){
                            strSecondName = myStringArr[1] + " " + myStringArr[2] + " " + myStringArr[3] + " " + myStringArr[4] + " " + myStringArr[5]
                        }
                        else if (myStringArr.count>4){
                            strSecondName = myStringArr[1] + " " + myStringArr[2] + " " + myStringArr[3] + " " + myStringArr[4]
                        }
                        else if (myStringArr.count>3){
                            strSecondName = myStringArr[1] + " " + myStringArr[2] + " " + myStringArr[3]
                        }
                        else if (myStringArr.count>2){
                            strSecondName = myStringArr[1] + " " + myStringArr[2]
                        }else if (myStringArr.count>1){
                            strSecondName = myStringArr[1]
                        }
                        
                        //print("strFirstName = \(strFirstName)")
                        //print("strSecondName = \(strSecondName)")
                        
                        let newStringFirstName = strFirstName.replacingOccurrences(of: ".", with: " ")
                        let newStringLastName = strSecondName.replacingOccurrences(of: ".", with: " ")
                        
                        //print("newStringFirstName = \(newStringFirstName)")
                        //print("newStringLastName = \(newStringLastName)")
                       
                        let arrEmpListDetails = ["firstName": "\(newStringFirstName)", "lastName": "\(newStringLastName)", "employeeEmailID": "\(emailId as! String)","userId":"\(userID)","groupName":"\(groupName)","jobTitle":"\(jobTitle)","projectName":"\(projectName)"]
                        AppDelegate.appdelegateInstance().arrGetEmployeeNameList.append(arrEmpListDetails)
                    }
                }
            }
             if(limitValue > 2){
                let nc = NotificationCenter.default
                nc.post(name: Notification.Name("updateEmailList"), object: nil)
            }
        }
        
        task.resume()
    }
    class func requestGetMethod(){
        
        let session = URLSession.shared
        let url = URL(string: "http://35.239.166.141:8888/app")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
    }
}
extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
