//
//  ConstantManager.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 06/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//

import Foundation

public class ConstantManager{
    let imgList = ["Name.png","Location.png","Mobile.png","Email.png","id-card.png","Purpose.png","devices.png"]
    let titleList = ["Name","Location","Mobile","Email","Gov Id","Purpose","Device Type"]
}

struct AppServerLink {
    static let localServerLink = "http://35.239.166.141:8888";
    static let liveServerLink = ""
    
    static let appVersion = "\(localServerLink)/app";
    static let imageUploadLink = "\(localServerLink)/api/file";
    static let createANewMeetGuest = "\(localServerLink)/api/guest";
    static let getGuestMeetByMeetCode = "\(localServerLink)/api/guest/meet";
    static let updateStatusMeetByMeetID = "\(localServerLink)/api/guest/status";
}


struct CharactersVaildations {
    
  static  let VIETNAMESE_NAMECHARACTERS = "([0-9A-ZẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴ']+\\s?\\b){2,}";
    static let  VIETNAMESE_NAMECHARACTERS1 = "/[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u";
    
    static  let VIETNAMESE_FROMCHARACTERS = "([0-9A-ZẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴ']+\\s?\\b){2,},. ";
    
  static  let VIETNAMESE_EMAILCHARACTERS  = "([0-9A-ZẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴ']+\\s?\\b){2,}_.@";
    
  static let VIETNAMESE_PURPOSECHARACTERS
    = "ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴ ";
    
    static  let VIETNAMESE_MOBILECHARACTERS = "(09|01[2|6|8|9])+([0-9]{8})";
    
  static  let ACCEPTABLE_NAMECHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
  //static  let ACCEPTABLE_FROMCHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,. "
  static  let ACCEPTABLE_COMPANYCHARATERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,. "
  static  let ACCEPTABLE_MOBILECHARACTERS = "0123456789"
  static  let ACCEPTABLE_EMAILCHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@"
  static  let ACCEPTABLE_GOVTIDCHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

  static  let ACCEPTABLE_PURPOSECHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
}

struct RadioButtonImages {
    static let RadioButtonUnSelect = "CheckBoxUnSelect.png"
    static let RadioButonSelect = "CheckBoxSelect.png"
}


struct StarImages{

    static let starWhite = "StarWhite.png"
    static let starGreen = "StarGreen.png"
    static let starRed = "StarRed.png"
    static let starYellow = "StarYellow.png"
    
    static let InputErrorImageRed = "StarRed.png"
    static let InputErrorImageGreen = "StarGreen.png"
}

 
