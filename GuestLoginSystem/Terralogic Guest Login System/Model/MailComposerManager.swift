
//
//  MailComposerManager.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 28/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//
import UIKit
import Foundation

class MailComposerManager{
    
    class func sendEMailInBackground(getGuestList:GuestDetailsModel){
        
        let smtpSession = MCOSMTPSession()
        smtpSession.hostname = "smtp.gmail.com"
        smtpSession.username = "noreply-tlguestapp@terralogic.com"
        smtpSession.password = "Guestapp@123"
        smtpSession.port = 465
        smtpSession.authType = MCOAuthType.saslPlain
        
        smtpSession.connectionType = MCOConnectionType.TLS
        smtpSession.connectionLogger = {(connectionID, type, data) in
            if data != nil {
                if let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                    NSLog("Connectionlogger: \(string)")
                }
            }
        }
        
        let guestFirstName = getGuestList.guestFirstName
        let guestLastName = getGuestList.guestLastName
        let mobileNumber = getGuestList.mobileNumber
        let guestEmailId = getGuestList.guestEmailId
        let companyName = getGuestList.companyName
        let sponsorName = getGuestList.sponsorName
        let purpose = getGuestList.purpose
        let fullName = getGuestList.guestName
        let imageName =  "\(String(describing: getGuestList.guestImageName!))"

        let builder = MCOMessageBuilder()
        builder.header.to = [MCOAddress(displayName: fullName, mailbox: getGuestList.empMailID)]
        builder.header.from = MCOAddress(displayName: NSLocalizedString("Guest Login System", comment: ""), mailbox: "noreply-tlguestapp@terralogic.com")
        builder.header.cc = [MCOAddress(displayName: NSLocalizedString("Operation Head", comment: ""), mailbox: "ngan.pham@terralogic.com")]
        //builder.header.cc = [MCOAddress(displayName: "Operation Head", mailbox: "operations@terralogic.com")]
        
        let txtHello = NSLocalizedString("Hello there,", comment: "")
        let txtMessage = NSLocalizedString("Please find the new guest details who is waiting for you at Reception.", comment: "")
        let txtFirstName = NSLocalizedString("First_Name", comment: "")
        let txtLastName = NSLocalizedString("Last_Name", comment: "")
        let txtMobileNo = NSLocalizedString("Mobile Number", comment: "")
        let txtEmailID = NSLocalizedString("Email ID", comment: "")
        let txtCompanyName = NSLocalizedString("Company_Name", comment: "")
        let txtPurpose = NSLocalizedString("Purpose", comment: "")
        let txtToMeet = NSLocalizedString("To_Meet", comment: "")
        let txtThankyou = NSLocalizedString("Thank you,", comment: "")
        let txtGuestLoginSystem = NSLocalizedString("Guest Login System.", comment: "")
        
        let msgContent:String = "<h3>\(txtHello)\n</h3><h4>\(txtMessage)</h4>"

        let path = FileManagers().getSavedImagePath(named: imageName)
      
        let imageHtmlCode = "<td><th><img src=\"cid:\(imageName)\" width=150 height=150 alt=\"Red dot\"/></th></td>"
        
         let imageHtmlCodeTable = "<div><table style=float: left; border=1 cellpadding=10 cellspacing=0 border-spacing:0 5px;><table><tr>\(imageHtmlCode)</tr></table></div>"
        
        let htmlBodyMsgTable = "<!DOCTYPE html><html><body>\(String(describing: msgContent))\n \(imageHtmlCodeTable) <br>\n\n\n <div><table style=float: left; border=1 cellpadding=10 cellspacing=0><tr><th>\(txtFirstName)</th><td align=centre>\(String(describing: guestFirstName ?? ""))</td></tr><tr><th>\(txtLastName)</th><td align=centre>\(String(describing: guestLastName ?? ""))</td></tr><tr><th>\(txtMobileNo)</th><td align=centre>\(String(describing: mobileNumber ?? ""))</td></tr><tr><th>\(txtEmailID)</th><td align=centre>\(String(describing: guestEmailId ?? ""))</td></tr><tr><th>\(txtCompanyName)</th><td align=centre>\(String(describing: companyName ?? ""))</td></tr><tr><th>\(txtPurpose)</th><td align=centre>\(String(describing: purpose ?? ""))</td></tr><tr><th>\(txtToMeet)</th><td align=centre>\(String(describing: sponsorName ?? ""))</td></table></div>\n<h4>\(txtThankyou)</h4>\n<h4>\(txtGuestLoginSystem)</h4></body></html>"

        //print("Check the html string = \(htmlBodyMsgTable)")
        
        builder.header.subject = NSLocalizedString("New Guest at Reception...", comment: "")
        builder.htmlBody = htmlBodyMsgTable
        
        let attachment = MCOAttachment()
        attachment.mimeType =  "image/png"
        attachment.filename = imageName
        
        if FileManagers().checkFileExistOrNot(fileName: imageName){
            print("File Exist in directory")
        }else{
            print("File Not Exist in directory")
        }
        
        var getImageData: NSData? = nil
        if let cert = NSData(contentsOfFile: path) {
            getImageData = cert
            let temp = MCOAttachment(data: getImageData! as Data, filename: imageName)
            temp?.isInlineAttachment = true
            attachment.isInlineAttachment = true
            attachment.data = getImageData! as Data
            attachment.contentID = imageName
            attachment.contentTypeParameterValue(forName: "image/png")
            attachment.filename = imageName
            builder.addRelatedAttachment(attachment)
        }
        
        builder.htmlBodyRendering()
        
        let rfc822Data = builder.data()
        
        //FileManagers().deleteFileFromDirectory(fileName: imageName)
        
        let sendOperation = smtpSession.sendOperation(with: rfc822Data)
        sendOperation?.start { (error) -> Void in
            if (error != nil) {
                 print("Error sending email: \(String(describing: error))")
             } else {
                 print("Successfully sent email!")
            }
        }
    }
}
