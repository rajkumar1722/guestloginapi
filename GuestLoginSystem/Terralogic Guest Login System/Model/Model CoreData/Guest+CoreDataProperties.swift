//
//  Guest+CoreDataProperties.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 06/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//
//

import Foundation
import CoreData


extension Guest {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Guest> {
        return NSFetchRequest<Guest>(entityName: "Guest")
    }

    @NSManaged public var comingFrom: String?
    @NSManaged public var empMailID: String?
    @NSManaged public var govtTaxID: String?
    @NSManaged public var guestEmailId: String?
    @NSManaged public var guestImage: String?
    @NSManaged public var guestName: String?
    @NSManaged public var mobileNumber: String?
    @NSManaged public var purpose: String?
    @NSManaged public var todayDate: String?
    @NSManaged public var guestID: String?

}
