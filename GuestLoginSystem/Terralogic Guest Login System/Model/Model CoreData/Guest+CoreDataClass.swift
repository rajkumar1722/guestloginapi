//
//  Guest+CoreDataClass.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 06/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit

@objc(Guest)
public class Guest: NSManagedObject {

    @NSManaged private var base64: String
    
    
    var image: UIImage? {
        set {
            base64 = base64(from: image!)
        }
        get {
            return image(from: base64)
        }
    }
    //MARK: - Insert Record into Coredata
    class func insertGuestRecords(guestObj: GuestDetailsModel) -> Void {
        
        let appdelegate = (AppDelegate.appdelegateInstance())
        let context = appdelegate.persistentContainer.viewContext
        let guestObject = NSEntityDescription.insertNewObject(forEntityName: "Guest", into: context) as! Guest
        guestObject.guestName = guestObj.guestName
        guestObject.comingFrom =  guestObj.comingFrom
        guestObject.mobileNumber = guestObj.mobileNumber
        guestObject.guestEmailId = guestObj.guestEmailId
        guestObject.govtTaxID = guestObj.govtTaxID
        guestObject.purpose = guestObj.purpose
        guestObject.todayDate =  guestObj.todayDate
        guestObject.guestID = guestObj.guestID
        guestObject.guestImage = guestObj.guestImage
        guestObject.empMailID = guestObj.empMailID
        do {
            print("Data Saved Successfully!!")
            try context.save()
        } catch {
            print("Data Saved Not Saved!!")
        }
    }
    //MARK: - Read Record from Coredata
    class func fetchGuestList() -> NSArray {
        
        let appdelegate = (AppDelegate.appdelegateInstance())
        let context = appdelegate.persistentContainer.viewContext
        let request = NSFetchRequest<Guest>(entityName: "Guest")
          request.returnsObjectsAsFaults = false
        var getGuestLists = [Any]()
        do {
            let result = try context.fetch(request)
//            print("result \(result)")
            for data in result as [NSManagedObject] {
                getGuestLists.append(data)
            }
        }
        catch { print(error) }
        return getGuestLists as NSArray
    }
    //MARK: - Delete All Records from Guest table in Coredata
   class func deleteAllRecords() {
        let appdelegate = (AppDelegate.appdelegateInstance())
        let context = appdelegate.persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Guest")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    class func readFirstRecordFromTable()-> NSArray{
        
        let appdelegate = (AppDelegate.appdelegateInstance())
        let context = appdelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Guest")
        request.sortDescriptors = [NSSortDescriptor(key: "TimeStamp", ascending: false)]
        request.fetchLimit = 1
        request.returnsObjectsAsFaults = false
        var getGuestLists = [Any]()
        do {
            // Execute Fetch Request
            let records = try context.fetch(request)
            if let records = records as? [NSManagedObject] {
                getGuestLists = records
            }
        } catch {
            print("Unable to fetch managed objects for entity \(String(describing: entity)).")
        }
        return getGuestLists as NSArray
    }
    
    class func filteredRecords()-> Array<Any>{
        
//        print(self.appdel.fromdate) // prints 2015-09-25
//        print(self.appdel.todate) // prints 2015-09-26
        
        //let fromdate =  "" //\(self.appdel.fromdate) 00:00" // add hours and mins to fromdate
        //let todate = DateManager.getTodayDate() //\(self.appdel.todate) 23:59" // add hours and mins to todate
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone(name: "GMT")! as TimeZone // this line resolved me the issue of getting one day less than the selected date
        //let startDate:NSDate = dateFormatter.date(from: fromdate)! as NSDate
        //let endDate:NSDate = dateFormatter.date(from: todate)! as NSDate
        
        let appdelegate = (AppDelegate.appdelegateInstance())
        let context = appdelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Guest")
        request.returnsObjectsAsFaults = false
        //"(date >= %@) AND (date <= %@)"
        request.predicate = NSPredicate(format: "(todayDate <= 2018-09-20)")
        request.sortDescriptors = [NSSortDescriptor(key: "todayDate", ascending: false)]
        //let results : NSArray
        var results = [Any]()
        do {
            let records = try context.fetch(request)
            if let records = records as? [NSManagedObject] {
                results = records
            }
        } catch {
            print ("There was an error")
        }
        return results
    }
}
extension Guest {
    
    func base64(from image: UIImage) -> String {
        // ************************************** //
        // UIImage to base64 encoding logic here! //
        // ************************************** //
        return "base64-encoded-string"
    }
    
    func image(from base64: String) -> UIImage {
        // ************************************** //
        // base64 to UIImage decoding logic here! //
        // ************************************** //
        return UIImage()
    }
}
