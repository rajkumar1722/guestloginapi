//
//  EmployeesList+CoreDataProperties.swift
//  Terralogic Guest Login System
//
//  Created by Satya-MINI on 11/09/18.
//  Copyright © 2018 Terralogic. All rights reserved.
//
//

import Foundation
import CoreData


extension EmployeesList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EmployeesList> {
        return NSFetchRequest<EmployeesList>(entityName: "EmployeesList")
    }

    @NSManaged public var name: String?
    @NSManaged public var emailID: String?
    @NSManaged public var psiID: Int16

}
